-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Servidor: localhost:8889
-- Tiempo de generación: 27-03-2016 a las 23:46:09
-- Versión del servidor: 5.5.38
-- Versión de PHP: 5.5.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `pacifica`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accion`
--

CREATE TABLE `accion` (
`IdAccion` int(10) unsigned NOT NULL,
  `Nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `Url` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `Activa` int(1) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `accion`
--

INSERT INTO `accion` (`IdAccion`, `Nombre`, `Url`, `Activa`) VALUES
(1, 'Home -> Cambiar contraseña', 'usuario/changepassword', 1),
(2, 'Home -> Cerrar sesión', 'site/logout', 1),
(3, 'Administración -> Localidades', 'localidad/index', 1),
(4, 'Administración -> Localidades -> Agregar', 'localidad/create', 1),
(5, 'Administración -> Localidades -> Editar', 'localidad/update', 1),
(6, 'Administración -> Localidades -> Eliminar', 'localidad/delete', 1),
(7, 'Seguridad -> Acciones', 'accion/index', 1),
(8, 'Seguridad -> Acciones -> Agregar', 'accion/create', 1),
(9, 'Seguridad -> Acciones -> Editar', 'accion/update', 1),
(10, 'Seguridad -> Acciones -> Eliminar', 'accion/delete', 1),
(11, 'Seguridad -> Roles', 'rol/index', 1),
(12, 'Seguridad -> Roles -> Agregar', 'rol/create', 1),
(13, 'Seguridad -> Roles -> Editar', 'rol/update', 1),
(14, 'Seguridad -> Roles -> Eliminar', 'rol/delete', 1),
(15, 'Seguridad -> Usuarios', 'usuario/index', 1),
(16, 'Seguridad -> Usuarios -> Agregar', 'usuario/create', 1),
(17, 'Seguridad -> Usuarios -> Editar', 'usuario/update', 1),
(18, 'Seguridad -> Usuarios -> Eliminar', 'usuario/delete', 1),
(19, 'Seguridad -> Usuarios -> Reiniciar contraseña', 'usuario/password', 1),
(20, 'Administracion', 'administracion/index', 1),
(21, 'Seguridad -> Usuarios -> Mostrar', 'usuarios/view', 1),
(22, 'Deposito->Producto', 'producto/index', 1),
(23, 'Deposito -> Marca', 'marca/index', 1),
(24, 'Deposito -> Marca -> Agregar', 'marca/create', 1),
(25, 'Deposito -> Marca -> Editar', 'marca/update', 1),
(26, 'Deposito -> Marca -> Eliminar', 'marca/delete', 1),
(27, 'Deposito -> Tipo Producto -> Agregar', 'tipo-producto/create', 1),
(28, 'Deposito -> Tipo Producto', 'tipo-producto/index', 1),
(29, 'Deposito -> Tipo Producto -> Editar', 'tipo-producto/update', 1),
(30, 'Deposito -> Tipo Producto -> Eliminar', 'tipo-producto/delete', 1),
(31, 'Deposito -> Proveedor -> Agregar', 'proveedor/create', 1),
(32, 'Deposito -> Proveedor', 'proveedor/index', 1),
(33, 'Deposito -> Proveedor -> Editar', 'proveedor/update', 1),
(34, 'Deposito -> Proveedor -> Eliminar', 'proveedor/delete', 1),
(36, 'Movimientos', 'movimientoproducto/index', 1),
(37, 'Deposito -> Existencia', 'existencia/index', 1),
(38, 'Remito -> Eliminar', 'remito/delete', 1),
(39, 'Deposito -> Ruta', 'destino/index', 1),
(40, 'Deposito -> Ruta -> Agregar', 'destino/create', 1),
(41, 'Deposito -> Ruta -> Editar', 'destino/update', 1),
(42, 'Deposito -> Ruta -> Eliminar', 'destino/delete', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria`
--

CREATE TABLE `auditoria` (
`IdAuditoria` int(10) unsigned NOT NULL,
  `IdUsuario` int(10) unsigned NOT NULL,
  `FechaHora` datetime NOT NULL,
  `Ip` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `SessionId` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `Tabla` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `PrimaryKey` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `TipoMovimiento` varchar(1) COLLATE utf8_spanish_ci NOT NULL,
  `DatoAnterior` text COLLATE utf8_spanish_ci,
  `DatoNuevo` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `auditoria`
--

INSERT INTO `auditoria` (`IdAuditoria`, `IdUsuario`, `FechaHora`, `Ip`, `SessionId`, `Tabla`, `PrimaryKey`, `TipoMovimiento`, `DatoAnterior`, `DatoNuevo`) VALUES
(9, 1, '2016-03-02 12:22:08', '::1', 'e30fdda2f4d60e6a7d88982857e3fe5c', 'rolaccion', '4||36', 'I', NULL, 'IdRol==4||IdAccion==36'),
(10, 1, '2016-03-02 12:22:08', '::1', 'e30fdda2f4d60e6a7d88982857e3fe5c', 'rolaccion', '4||2', 'I', NULL, 'IdRol==4||IdAccion==2'),
(11, 1, '2016-03-02 12:22:08', '::1', 'e30fdda2f4d60e6a7d88982857e3fe5c', 'rolaccion', '4||1', 'I', NULL, 'IdRol==4||IdAccion==1'),
(12, 1, '2016-03-07 14:23:25', '192.168.0.26', 'u87c1rf32uf21r18gb4di4rd12', 'usuario', '3', 'U', 'IdUsuario==3||Apellido==Juan||Nombre==Perez||Foto==||Username==oper||Password==fd154ffe305c26b5004231ff709bd1b8||IdRol==4||Activo==1||IdDestino==5', 'IdUsuario==3||Apellido==arias||Nombre==sergio||Foto==||Username==ruta15||Password==fd154ffe305c26b5004231ff709bd1b8||IdRol==4||Activo==1||IdDestino==4'),
(13, 1, '2016-03-07 14:23:48', '192.168.0.26', 'u87c1rf32uf21r18gb4di4rd12', 'usuario', '3', 'U', 'IdUsuario==3||Apellido==arias||Nombre==sergio||Foto==||Username==ruta15||Password==fd154ffe305c26b5004231ff709bd1b8||IdRol==4||Activo==1||IdDestino==4', 'IdUsuario==3||Apellido==arias||Nombre==sergio||Foto==||Username==ruta15||Password==e10adc3949ba59abbe56e057f20f883e||IdRol==4||Activo==1||IdDestino==4'),
(14, 1, '2016-03-07 14:24:19', '192.168.0.26', 'u87c1rf32uf21r18gb4di4rd12', 'usuario', '4', 'U', 'IdUsuario==4||Apellido==Carlos||Nombre==Roberto||Foto==||Username==operador2||Password==a055bdff174434bd915165a5a4422201||IdRol==4||Activo==1||IdDestino==3', 'IdUsuario==4||Apellido==juan jose||Nombre==vivanco||Foto==||Username==ruta51||Password==a055bdff174434bd915165a5a4422201||IdRol==4||Activo==1||IdDestino==6'),
(15, 1, '2016-03-07 14:24:26', '192.168.0.26', 'u87c1rf32uf21r18gb4di4rd12', 'usuario', '4', 'U', 'IdUsuario==4||Apellido==juan jose||Nombre==vivanco||Foto==||Username==ruta51||Password==a055bdff174434bd915165a5a4422201||IdRol==4||Activo==1||IdDestino==6', 'IdUsuario==4||Apellido==juan jose||Nombre==vivanco||Foto==||Username==ruta51||Password==e10adc3949ba59abbe56e057f20f883e||IdRol==4||Activo==1||IdDestino==6'),
(16, 1, '2016-03-07 14:25:05', '192.168.0.26', 'u87c1rf32uf21r18gb4di4rd12', 'usuario', '5', 'I', NULL, 'IdUsuario==||Apellido==palacios||Nombre==esteban||Foto==||Username==ruta22||Password==e10adc3949ba59abbe56e057f20f883e||IdRol==4||Activo==1||IdDestino==5'),
(17, 1, '2016-03-07 14:25:51', '192.168.0.26', 'u87c1rf32uf21r18gb4di4rd12', 'usuario', '6', 'I', NULL, 'IdUsuario==||Apellido==cabrera||Nombre==martin||Foto==||Username==ruta4||Password==e10adc3949ba59abbe56e057f20f883e||IdRol==4||Activo==1||IdDestino==3'),
(18, 1, '2016-03-07 14:26:22', '192.168.0.26', 'u87c1rf32uf21r18gb4di4rd12', 'usuario', '7', 'I', NULL, 'IdUsuario==||Apellido==lahora||Nombre==matias||Foto==||Username==ruta3||Password==e10adc3949ba59abbe56e057f20f883e||IdRol==4||Activo==1||IdDestino==2'),
(19, 1, '2016-03-07 14:27:51', '192.168.0.26', 'cch2cbck4udq809u4u6548j252', 'usuario', '8', 'I', NULL, 'IdUsuario==||Apellido==lavin||Nombre==juan carlos||Foto==||Username==tecnica||Password==e10adc3949ba59abbe56e057f20f883e||IdRol==4||Activo==1||IdDestino==1'),
(20, 1, '2016-03-22 04:19:40', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||38', 'I', NULL, 'IdRol==2||IdAccion==38'),
(21, 1, '2016-03-22 04:19:40', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||36', 'I', NULL, 'IdRol==2||IdAccion==36'),
(22, 1, '2016-03-22 04:19:40', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||2', 'I', NULL, 'IdRol==2||IdAccion==2'),
(23, 1, '2016-03-22 04:19:41', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||1', 'I', NULL, 'IdRol==2||IdAccion==1'),
(24, 1, '2016-03-22 04:19:41', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||22', 'I', NULL, 'IdRol==2||IdAccion==22'),
(25, 1, '2016-03-22 04:19:41', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||30', 'I', NULL, 'IdRol==2||IdAccion==30'),
(26, 1, '2016-03-22 04:19:41', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||29', 'I', NULL, 'IdRol==2||IdAccion==29'),
(27, 1, '2016-03-22 04:19:41', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||27', 'I', NULL, 'IdRol==2||IdAccion==27'),
(28, 1, '2016-03-22 04:19:41', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||28', 'I', NULL, 'IdRol==2||IdAccion==28'),
(29, 1, '2016-03-22 04:19:41', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||34', 'I', NULL, 'IdRol==2||IdAccion==34'),
(30, 1, '2016-03-22 04:19:41', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||33', 'I', NULL, 'IdRol==2||IdAccion==33'),
(31, 1, '2016-03-22 04:19:41', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||31', 'I', NULL, 'IdRol==2||IdAccion==31'),
(32, 1, '2016-03-22 04:19:41', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||32', 'I', NULL, 'IdRol==2||IdAccion==32'),
(33, 1, '2016-03-22 04:19:41', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||26', 'I', NULL, 'IdRol==2||IdAccion==26'),
(34, 1, '2016-03-22 04:19:42', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||25', 'I', NULL, 'IdRol==2||IdAccion==25'),
(35, 1, '2016-03-22 04:19:42', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||24', 'I', NULL, 'IdRol==2||IdAccion==24'),
(36, 1, '2016-03-22 04:19:42', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||23', 'I', NULL, 'IdRol==2||IdAccion==23'),
(37, 1, '2016-03-22 04:19:42', '::1', 'e9c401d576e323fb4ab6ce0063795649', 'rolaccion', '2||37', 'I', NULL, 'IdRol==2||IdAccion==37'),
(38, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||38', 'I', NULL, 'IdRol==2||IdAccion==38'),
(39, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||36', 'I', NULL, 'IdRol==2||IdAccion==36'),
(40, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||2', 'I', NULL, 'IdRol==2||IdAccion==2'),
(41, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||1', 'I', NULL, 'IdRol==2||IdAccion==1'),
(42, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||22', 'I', NULL, 'IdRol==2||IdAccion==22'),
(43, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||30', 'I', NULL, 'IdRol==2||IdAccion==30'),
(44, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||29', 'I', NULL, 'IdRol==2||IdAccion==29'),
(45, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||27', 'I', NULL, 'IdRol==2||IdAccion==27'),
(46, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||28', 'I', NULL, 'IdRol==2||IdAccion==28'),
(47, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||42', 'I', NULL, 'IdRol==2||IdAccion==42'),
(48, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||41', 'I', NULL, 'IdRol==2||IdAccion==41'),
(49, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||40', 'I', NULL, 'IdRol==2||IdAccion==40'),
(50, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||39', 'I', NULL, 'IdRol==2||IdAccion==39'),
(51, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||34', 'I', NULL, 'IdRol==2||IdAccion==34'),
(52, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||33', 'I', NULL, 'IdRol==2||IdAccion==33'),
(53, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||31', 'I', NULL, 'IdRol==2||IdAccion==31'),
(54, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||32', 'I', NULL, 'IdRol==2||IdAccion==32'),
(55, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||26', 'I', NULL, 'IdRol==2||IdAccion==26'),
(56, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||25', 'I', NULL, 'IdRol==2||IdAccion==25'),
(57, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||24', 'I', NULL, 'IdRol==2||IdAccion==24'),
(58, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||23', 'I', NULL, 'IdRol==2||IdAccion==23'),
(59, 1, '2016-03-26 23:41:02', '::1', '96420332b54e4bbcb560f7b6a7e29189', 'rolaccion', '2||37', 'I', NULL, 'IdRol==2||IdAccion==37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destino`
--

CREATE TABLE `destino` (
`IdDestino` int(10) unsigned NOT NULL,
  `Destino` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `destino`
--

INSERT INTO `destino` (`IdDestino`, `Destino`) VALUES
(1, 'Deposito'),
(3, 'Ruta 4'),
(4, 'Ruta 15'),
(5, 'Ruta 22'),
(6, 'Ruta 51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `existencia`
--

CREATE TABLE `existencia` (
`IdExistencia` int(11) NOT NULL,
  `IdProducto` int(11) NOT NULL,
  `Precio` decimal(11,2) NOT NULL,
  `Vencimiento` date NOT NULL,
  `Cantidad` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `existencia`
--

INSERT INTO `existencia` (`IdExistencia`, `IdProducto`, `Precio`, `Vencimiento`, `Cantidad`) VALUES
(1, 6, 0.00, '0000-00-00', 600),
(2, 136, 0.00, '0000-00-00', 18),
(3, 137, 0.00, '0000-00-00', 1600),
(4, 138, 0.00, '0000-00-00', 175),
(5, 139, 0.00, '0000-00-00', 834),
(6, 140, 0.00, '0000-00-00', 688),
(7, 141, 0.00, '0000-00-00', 123000),
(8, 142, 0.00, '0000-00-00', 480),
(9, 143, 0.00, '0000-00-00', 11),
(10, 144, 0.00, '0000-00-00', 130),
(11, 145, 0.00, '0000-00-00', 3636),
(12, 146, 0.00, '0000-00-00', 2400),
(13, 148, 0.00, '0000-00-00', 187),
(14, 173, 0.00, '0000-00-00', 3),
(15, 97, 0.00, '0000-00-00', 140),
(16, 3, 0.00, '0000-00-00', 30),
(17, 4, 0.00, '0000-00-00', 27),
(18, 156, 0.00, '0000-00-00', 51),
(19, 5, 0.00, '0000-00-00', 48),
(20, 7, 0.00, '0000-00-00', 435),
(21, 8, 0.00, '0000-00-00', 129),
(22, 14, 0.00, '0000-00-00', 52),
(23, 15, 0.00, '0000-00-00', 49),
(24, 18, 0.00, '0000-00-00', 6),
(25, 19, 0.00, '0000-00-00', 5),
(26, 20, 0.00, '0000-00-00', 5),
(27, 21, 0.00, '0000-00-00', 5),
(28, 30, 0.00, '0000-00-00', 51),
(29, 32, 0.00, '0000-00-00', 200),
(30, 35, 0.00, '0000-00-00', 174),
(31, 37, 0.00, '0000-00-00', 410),
(32, 38, 0.00, '0000-00-00', 27),
(33, 39, 0.00, '0000-00-00', 36),
(34, 41, 0.00, '0000-00-00', 69),
(35, 42, 0.00, '0000-00-00', 108),
(36, 46, 0.00, '0000-00-00', 370),
(37, 160, 0.00, '0000-00-00', 9),
(38, 159, 0.00, '0000-00-00', 9),
(39, 157, 0.00, '0000-00-00', 10),
(40, 158, 0.00, '0000-00-00', 9),
(41, 165, 0.00, '0000-00-00', 140),
(42, 150, 0.00, '0000-00-00', 350),
(43, 149, 0.00, '0000-00-00', 180),
(44, 151, 0.00, '0000-00-00', 360),
(45, 152, 0.00, '0000-00-00', 290),
(46, 153, 0.00, '0000-00-00', 180),
(47, 154, 0.00, '0000-00-00', 360),
(48, 155, 0.00, '0000-00-00', 20),
(49, 163, 0.00, '0000-00-00', 16),
(50, 164, 0.00, '0000-00-00', 9),
(51, 169, 0.00, '0000-00-00', 92),
(52, 50, 0.00, '0000-00-00', 100),
(53, 51, 0.00, '0000-00-00', 13),
(54, 52, 0.00, '0000-00-00', 6),
(55, 53, 0.00, '0000-00-00', 5),
(56, 54, 0.00, '0000-00-00', 4),
(57, 55, 0.00, '0000-00-00', 4),
(58, 56, 0.00, '0000-00-00', 7),
(59, 57, 0.00, '0000-00-00', 7),
(60, 58, 0.00, '0000-00-00', 8),
(61, 59, 0.00, '0000-00-00', 23),
(62, 60, 0.00, '0000-00-00', 6),
(63, 61, 0.00, '0000-00-00', 4),
(64, 62, 0.00, '0000-00-00', 7),
(65, 63, 0.00, '0000-00-00', 4),
(66, 64, 0.00, '0000-00-00', 1),
(67, 65, 0.00, '0000-00-00', 3),
(68, 66, 0.00, '0000-00-00', 3),
(69, 67, 0.00, '0000-00-00', 3),
(70, 69, 0.00, '0000-00-00', 6),
(71, 70, 0.00, '0000-00-00', 6),
(72, 71, 0.00, '0000-00-00', 19),
(73, 72, 0.00, '0000-00-00', 15),
(74, 73, 0.00, '0000-00-00', 15),
(75, 75, 0.00, '0000-00-00', 8),
(76, 76, 0.00, '0000-00-00', 3),
(77, 162, 0.00, '0000-00-00', 3),
(78, 77, 0.00, '0000-00-00', 1),
(79, 79, 0.00, '0000-00-00', 5),
(80, 80, 0.00, '0000-00-00', 369),
(81, 81, 0.00, '0000-00-00', 21),
(82, 83, 0.00, '0000-00-00', 84),
(83, 84, 0.00, '0000-00-00', 118),
(84, 85, 0.00, '0000-00-00', 105),
(85, 86, 0.00, '0000-00-00', 72),
(86, 87, 0.00, '0000-00-00', 282),
(87, 88, 0.00, '0000-00-00', 413),
(88, 89, 0.00, '0000-00-00', 292),
(89, 90, 0.00, '0000-00-00', 472),
(90, 166, 0.00, '0000-00-00', 1),
(91, 167, 0.00, '0000-00-00', 84),
(92, 168, 0.00, '0000-00-00', 21),
(93, 91, 0.00, '0000-00-00', 306),
(94, 92, 0.00, '0000-00-00', 410),
(95, 93, 0.00, '0000-00-00', 348),
(96, 94, 0.00, '0000-00-00', 4),
(97, 95, 0.00, '0000-00-00', 4),
(98, 96, 0.00, '0000-00-00', 5),
(99, 98, 0.00, '0000-00-00', 266),
(100, 99, 0.00, '0000-00-00', 100),
(101, 101, 0.00, '0000-00-00', 71),
(102, 102, 0.00, '0000-00-00', 86),
(103, 103, 0.00, '0000-00-00', 240),
(104, 105, 0.00, '0000-00-00', 130),
(105, 107, 0.00, '0000-00-00', 296),
(106, 108, 0.00, '0000-00-00', 48),
(107, 110, 0.00, '0000-00-00', 33),
(108, 111, 0.00, '0000-00-00', 57),
(109, 112, 0.00, '0000-00-00', 440),
(110, 113, 0.00, '0000-00-00', 12),
(111, 114, 0.00, '0000-00-00', 134),
(112, 117, 0.00, '0000-00-00', 1),
(113, 118, 0.00, '0000-00-00', 7),
(114, 119, 0.00, '0000-00-00', 8),
(115, 120, 0.00, '0000-00-00', 48),
(116, 121, 0.00, '0000-00-00', 48),
(117, 122, 0.00, '0000-00-00', 17),
(118, 124, 0.00, '0000-00-00', 208),
(119, 125, 0.00, '0000-00-00', 40),
(120, 126, 0.00, '0000-00-00', 24),
(121, 127, 0.00, '0000-00-00', 240),
(122, 128, 0.00, '0000-00-00', 72),
(123, 129, 0.00, '0000-00-00', 90),
(124, 131, 0.00, '0000-00-00', 24),
(125, 132, 0.00, '0000-00-00', 48),
(126, 133, 0.00, '0000-00-00', 90),
(127, 170, 0.00, '0000-00-00', 216),
(128, 135, 0.00, '0000-00-00', 90),
(129, 171, 0.00, '0000-00-00', 72),
(130, 189, 0.00, '0000-00-00', 12),
(131, 190, 0.00, '0000-00-00', 16),
(132, 192, 0.00, '0000-00-00', 24),
(133, 193, 0.00, '0000-00-00', 12),
(134, 194, 0.00, '0000-00-00', 6),
(135, 195, 0.00, '0000-00-00', 6),
(136, 196, 0.00, '0000-00-00', 6),
(137, 197, 0.00, '0000-00-00', 14),
(138, 198, 0.00, '0000-00-00', 12),
(139, 202, 0.00, '0000-00-00', 200),
(140, 201, 0.00, '0000-00-00', 360),
(141, 200, 0.00, '0000-00-00', 240);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidad`
--

CREATE TABLE `localidad` (
  `IdLocalidad` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL,
  `IdProvincia` int(11) NOT NULL,
  `CodigoPostal` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `localidad`
--

INSERT INTO `localidad` (`IdLocalidad`, `Nombre`, `IdProvincia`, `CodigoPostal`) VALUES
(0, 'San Martin de los Andes', 14, '8370'),
(1, 'Neuquen', 14, '8300');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
`IdMarca` int(10) NOT NULL,
  `Marca` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`IdMarca`, `Marca`) VALUES
(1, 'Topline'),
(2, 'Bonafide'),
(3, 'Aguila'),
(4, 'bagley'),
(5, 'twistos'),
(6, 'Pepsico'),
(7, 'prueba'),
(8, 'Trio'),
(9, 'Ser'),
(10, 'Ledesma'),
(11, 'Oreo'),
(12, 'menthoplus'),
(13, 'cereal mix'),
(14, 'arcor'),
(15, '9 de oro'),
(16, 'Terrabusi'),
(17, 'Suchard'),
(18, 'hola'),
(19, 'taragui'),
(20, 'beldent'),
(21, 'mentos'),
(22, 'cofler'),
(23, 'mogul'),
(24, 'fantoche'),
(25, 'mondelez'),
(26, 'coca cola'),
(27, 'crisppino'),
(28, 'valido'),
(29, 'lulemuu');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
`IdMenu` int(10) unsigned NOT NULL,
  `Nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `IdAccion` int(10) unsigned DEFAULT NULL,
  `Activo` int(1) unsigned NOT NULL,
  `IdMenuPadre` int(10) unsigned DEFAULT NULL,
  `Orden` int(2) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`IdMenu`, `Nombre`, `IdAccion`, `Activo`, `IdMenuPadre`, `Orden`) VALUES
(1, 'Administración', 20, 1, NULL, 1),
(2, 'Seguridad', 7, 1, NULL, 2),
(3, 'Home', NULL, 1, NULL, 3),
(4, 'Cambiar contraseña', 1, 1, 3, 1),
(5, 'Cerrar sesión', 2, 1, 3, 2),
(6, 'Localidades', 3, 1, 1, 1),
(7, 'Acciones', 7, 1, 2, 1),
(8, 'Roles', 11, 1, 2, 2),
(9, 'Usuarios', 15, 1, 2, 3),
(10, 'Deposito', 22, 1, NULL, 1),
(11, 'Marca', 23, 1, 10, 2),
(12, 'Tipo Producto', 28, 1, 10, 3),
(13, 'Proveedor', 32, 1, 10, 4),
(14, 'Productos', 22, 1, 10, 5),
(16, 'Movimientos', 36, 1, NULL, 1),
(17, 'Existencia', 37, 1, 10, 7),
(18, 'Ruta', 39, 1, 10, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `motivo`
--

CREATE TABLE `motivo` (
`IdMotivo` int(11) NOT NULL,
  `Motivo` varchar(255) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `motivo`
--

INSERT INTO `motivo` (`IdMotivo`, `Motivo`) VALUES
(1, 'Cambio'),
(2, 'Merma'),
(3, 'Vencido');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientoproducto`
--

CREATE TABLE `movimientoproducto` (
`IdMovimiento` int(10) NOT NULL,
  `Fecha` date NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Precio` decimal(11,2) NOT NULL,
  `IdProducto` int(10) NOT NULL,
  `IdRemito` int(11) DEFAULT NULL,
  `TipoMovimiento` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 -Entrada ; 1 - Salida;2 - Devolucion',
  `IdDestino` int(10) DEFAULT NULL,
  `IdUsuario` int(10) NOT NULL,
  `Vencimiento` date DEFAULT NULL,
  `IdMotivo` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `movimientoproducto`
--

INSERT INTO `movimientoproducto` (`IdMovimiento`, `Fecha`, `Cantidad`, `Precio`, `IdProducto`, `IdRemito`, `TipoMovimiento`, `IdDestino`, `IdUsuario`, `Vencimiento`, `IdMotivo`) VALUES
(1, '2016-03-08', 300, 0.00, 6, NULL, 0, 1, 2, '2017-01-10', NULL),
(2, '2016-03-08', 100, 0.00, 6, NULL, 0, 1, 2, '2016-03-31', NULL),
(3, '2016-03-08', 200, 0.00, 6, NULL, 0, 1, 2, '2016-11-30', NULL),
(4, '2016-03-08', 18, 0.00, 136, NULL, 0, 1, 2, '2016-09-09', NULL),
(5, '2016-03-08', 1600, 0.00, 137, NULL, 0, 1, 2, '2016-10-13', NULL),
(6, '2016-03-08', 75, 0.00, 138, NULL, 0, 1, 2, '2016-07-31', NULL),
(7, '2016-03-08', 100, 0.00, 138, NULL, 0, 1, 2, '2016-10-31', NULL),
(8, '2016-03-08', 25, 0.00, 139, NULL, 0, 1, 2, '2016-11-30', NULL),
(9, '2016-03-08', 275, 0.00, 139, NULL, 0, 1, 2, '2016-11-30', NULL),
(10, '2016-03-08', 534, 0.00, 139, NULL, 0, 1, 2, '2016-08-31', NULL),
(11, '2016-03-08', 300, 0.00, 140, NULL, 0, 1, 2, '2017-01-31', NULL),
(12, '2016-03-08', 388, 0.00, 140, NULL, 0, 1, 2, '2016-10-31', NULL),
(13, '2016-03-08', 123000, 0.00, 141, NULL, 0, 1, 2, '2016-03-12', NULL),
(14, '2016-03-08', 485, 0.00, 142, NULL, 0, 1, 2, '2020-01-01', NULL),
(15, '2016-03-08', 11, 0.00, 143, NULL, 0, 1, 2, '2016-12-02', NULL),
(16, '2016-03-08', 130, 0.00, 144, NULL, 0, 1, 2, '2016-12-01', NULL),
(17, '2016-03-08', 3636, 0.00, 145, NULL, 0, 1, 2, '2016-03-19', NULL),
(18, '2016-03-08', 2400, 0.00, 146, NULL, 0, 1, 2, '2016-11-04', NULL),
(19, '2016-03-08', 187, 0.00, 148, NULL, 0, 1, 2, '2016-06-20', NULL),
(20, '2016-03-08', 3, 0.00, 173, NULL, 0, 1, 2, '2016-02-01', NULL),
(21, '2016-03-08', 140, 0.00, 97, NULL, 0, 1, 2, '2020-12-19', NULL),
(22, '2016-03-08', 30, 0.00, 3, NULL, 0, 1, 2, '2017-08-21', NULL),
(23, '2016-03-08', 30, 0.00, 4, NULL, 0, 1, 2, '2017-08-11', NULL),
(24, '2016-03-08', 51, 0.00, 156, NULL, 0, 1, 2, '2017-08-14', NULL),
(25, '2016-03-08', 48, 0.00, 5, NULL, 0, 1, 2, '2017-08-26', NULL),
(26, '2016-03-08', 435, 0.00, 7, NULL, 0, 1, 2, '2016-04-03', NULL),
(27, '2016-03-08', 141, 0.00, 8, NULL, 0, 1, 2, '2016-12-09', NULL),
(28, '2016-03-08', 52, 0.00, 14, NULL, 0, 1, 2, '2017-07-30', NULL),
(29, '2016-03-08', 49, 0.00, 15, NULL, 0, 1, 2, '2017-07-16', NULL),
(30, '2016-03-08', 6, 0.00, 18, NULL, 0, 1, 2, '2017-05-19', NULL),
(31, '2016-03-08', 5, 0.00, 19, NULL, 0, 1, 2, '2017-06-01', NULL),
(32, '2016-03-08', 5, 0.00, 20, NULL, 0, 1, 2, '2017-04-28', NULL),
(33, '2016-03-08', 5, 0.00, 21, NULL, 0, 1, 2, '2017-10-05', NULL),
(34, '2016-03-08', 63, 0.00, 30, NULL, 0, 1, 2, '2016-11-09', NULL),
(35, '2016-03-08', 200, 0.00, 32, NULL, 0, 1, 2, '2016-07-23', NULL),
(36, '2016-03-08', 186, 0.00, 35, NULL, 0, 1, 2, '2016-11-17', NULL),
(37, '2016-03-08', 426, 0.00, 37, NULL, 0, 1, 2, '2016-06-07', NULL),
(38, '2016-03-08', 27, 0.00, 38, NULL, 0, 1, 2, '2016-09-01', NULL),
(39, '2016-03-08', 36, 0.00, 39, NULL, 0, 1, 2, '2016-09-22', NULL),
(40, '2016-03-08', 84, 0.00, 41, NULL, 0, 1, 2, '2016-04-14', NULL),
(41, '2016-03-08', 108, 0.00, 42, NULL, 0, 1, 2, '2016-05-04', NULL),
(42, '2016-03-08', 380, 0.00, 46, NULL, 0, 1, 2, '2016-06-05', NULL),
(43, '2016-03-08', 9, 0.00, 160, NULL, 0, 1, 2, '2017-07-19', NULL),
(44, '2016-03-08', 9, 0.00, 159, NULL, 0, 1, 2, '2017-04-14', NULL),
(45, '2016-03-08', 10, 0.00, 157, NULL, 0, 1, 2, '2017-05-17', NULL),
(46, '2016-03-08', 9, 0.00, 158, NULL, 0, 1, 2, '2016-12-08', NULL),
(47, '2016-03-08', 140, 0.00, 165, NULL, 0, 1, 2, '2016-07-27', NULL),
(48, '2016-03-08', 360, 0.00, 150, NULL, 0, 1, 2, '2016-09-01', NULL),
(49, '2016-03-08', 180, 0.00, 149, NULL, 0, 1, 2, '2016-10-01', NULL),
(50, '2016-03-08', 360, 0.00, 151, NULL, 0, 1, 2, '2016-10-01', NULL),
(51, '2016-03-08', 300, 0.00, 152, NULL, 0, 1, 2, '2016-06-01', NULL),
(52, '2016-03-08', 180, 0.00, 153, NULL, 0, 1, 2, '2016-10-01', NULL),
(53, '2016-03-08', 360, 0.00, 154, NULL, 0, 1, 2, '2016-09-01', NULL),
(54, '2016-03-08', 20, 0.00, 155, NULL, 0, 1, 2, '2016-12-01', NULL),
(55, '2016-03-09', 5, 0.00, 142, NULL, 1, 6, 4, '2016-03-09', NULL),
(56, '2016-03-09', 16, 0.00, 163, NULL, 0, 1, 2, '2016-07-24', NULL),
(57, '2016-03-09', 9, 0.00, 164, NULL, 0, 1, 2, '2016-05-07', NULL),
(58, '2016-03-09', 92, 0.00, 169, NULL, 0, 1, 2, '2016-04-15', NULL),
(59, '2016-03-09', 100, 0.00, 50, NULL, 0, 1, 2, '2016-12-01', NULL),
(60, '2016-03-09', 13, 0.00, 51, NULL, 0, 1, 2, '2016-04-27', NULL),
(61, '2016-03-09', 6, 0.00, 52, NULL, 0, 1, 2, '2017-01-27', NULL),
(62, '2016-03-09', 5, 0.00, 53, NULL, 0, 1, 2, '2017-09-01', NULL),
(63, '2016-03-09', 4, 0.00, 54, NULL, 0, 1, 2, '2017-09-01', NULL),
(64, '2016-03-09', 4, 0.00, 55, NULL, 0, 1, 2, '2017-09-01', NULL),
(65, '2016-03-09', 7, 0.00, 56, NULL, 0, 1, 2, '2016-10-04', NULL),
(66, '2016-03-09', 7, 0.00, 57, NULL, 0, 1, 2, '2016-09-02', NULL),
(67, '2016-03-09', 8, 0.00, 58, NULL, 0, 1, 2, '2016-09-10', NULL),
(68, '2016-03-09', 23, 0.00, 59, NULL, 0, 1, 2, '2017-08-18', NULL),
(69, '2016-03-09', 6, 0.00, 60, NULL, 0, 1, 2, '2016-10-26', NULL),
(70, '2016-03-09', 4, 0.00, 61, NULL, 0, 1, 2, '2016-10-28', NULL),
(71, '2016-03-09', 7, 0.00, 62, NULL, 0, 1, 2, '2016-08-10', NULL),
(72, '2016-03-09', 4, 0.00, 63, NULL, 0, 1, 2, '2016-07-19', NULL),
(73, '2016-03-09', 1, 0.00, 64, NULL, 0, 1, 2, '2016-05-29', NULL),
(74, '2016-03-09', 3, 0.00, 65, NULL, 0, 1, 2, '2016-08-01', NULL),
(75, '2016-03-09', 3, 0.00, 66, NULL, 0, 1, 2, '2016-09-01', NULL),
(76, '2016-03-09', 3, 0.00, 67, NULL, 0, 1, 2, '2016-07-01', NULL),
(77, '2016-03-09', 6, 0.00, 69, NULL, 0, 1, 2, '2016-10-01', NULL),
(78, '2016-03-09', 6, 0.00, 70, NULL, 0, 1, 2, '2016-07-01', NULL),
(79, '2016-03-09', 15, 0.00, 71, NULL, 0, 1, 2, '2016-09-01', NULL),
(80, '2016-03-09', 4, 0.00, 71, NULL, 0, 1, 2, '2016-07-01', NULL),
(81, '2016-03-09', 15, 0.00, 72, NULL, 0, 1, 2, '2016-10-01', NULL),
(82, '2016-03-09', 15, 0.00, 73, NULL, 0, 1, 2, '2016-07-01', NULL),
(83, '2016-03-09', 8, 0.00, 75, NULL, 0, 1, 2, '2016-12-04', NULL),
(84, '2016-03-09', 3, 0.00, 76, NULL, 0, 1, 2, '2016-05-01', NULL),
(85, '2016-03-09', 3, 0.00, 162, NULL, 0, 1, 2, '2016-06-01', NULL),
(86, '2016-03-09', 1, 0.00, 77, NULL, 0, 1, 2, '2016-07-01', NULL),
(87, '2016-03-09', 5, 0.00, 79, NULL, 0, 1, 2, '2016-05-03', NULL),
(88, '2016-03-09', 192, 0.00, 80, NULL, 0, 1, 2, '2016-04-08', NULL),
(89, '2016-03-09', 192, 0.00, 80, NULL, 0, 1, 2, '2016-06-03', NULL),
(90, '2016-03-09', 21, 0.00, 81, NULL, 0, 1, 2, '2016-04-16', NULL),
(91, '2016-03-09', 84, 0.00, 83, NULL, 0, 1, 2, '2016-05-17', NULL),
(92, '2016-03-09', 108, 0.00, 84, NULL, 0, 1, 2, '2016-10-22', NULL),
(93, '2016-03-09', 117, 0.00, 85, NULL, 0, 1, 2, '2016-05-16', NULL),
(94, '2016-03-09', 72, 0.00, 86, NULL, 0, 1, 2, '2016-06-03', NULL),
(95, '2016-03-09', 192, 0.00, 87, NULL, 0, 1, 2, '2016-07-01', NULL),
(96, '2016-03-09', 422, 0.00, 88, NULL, 0, 1, 2, '2016-05-12', NULL),
(97, '2016-03-09', 300, 0.00, 89, NULL, 0, 1, 2, '2016-05-13', NULL),
(98, '2016-03-09', 294, 0.00, 90, NULL, 0, 1, 2, '2016-06-01', NULL),
(99, '2016-03-09', 1, 0.00, 166, NULL, 0, 1, 2, '2016-05-25', NULL),
(100, '2016-03-09', 84, 0.00, 167, NULL, 0, 1, 2, '2016-05-05', NULL),
(101, '2016-03-09', 21, 0.00, 168, NULL, 0, 1, 2, '2016-04-21', NULL),
(102, '2016-03-09', 216, 0.00, 91, NULL, 0, 1, 2, '2016-06-01', NULL),
(103, '2016-03-09', 282, 0.00, 92, NULL, 0, 1, 2, '2016-07-01', NULL),
(104, '2016-03-09', 303, 0.00, 93, NULL, 0, 1, 2, '2016-06-15', NULL),
(105, '2016-03-09', 4, 0.00, 94, NULL, 0, 1, 2, '2017-10-16', NULL),
(106, '2016-03-09', 4, 0.00, 95, NULL, 0, 1, 2, '2017-04-28', NULL),
(107, '2016-03-09', 5, 0.00, 96, NULL, 0, 1, 2, '2017-05-05', NULL),
(108, '2016-03-09', 260, 0.00, 98, NULL, 0, 1, 2, '2016-05-12', NULL),
(109, '2016-03-09', 16, 0.00, 98, NULL, 0, 1, 2, '2016-05-12', NULL),
(110, '2016-03-09', 100, 0.00, 99, NULL, 0, 1, 2, '2016-11-16', NULL),
(111, '2016-03-09', 71, 0.00, 101, NULL, 0, 1, 2, '2016-07-29', NULL),
(112, '2016-03-09', 86, 0.00, 102, NULL, 0, 1, 2, '2016-11-24', NULL),
(113, '2016-03-09', 240, 0.00, 103, NULL, 0, 1, 2, '2016-10-02', NULL),
(114, '2016-03-09', 163, 0.00, 105, NULL, 0, 1, 2, '2016-04-13', NULL),
(115, '2016-03-09', 312, 0.00, 107, NULL, 0, 1, 2, '2016-05-28', NULL),
(116, '2016-03-09', 54, 0.00, 108, NULL, 0, 1, 2, '2016-06-27', NULL),
(117, '2016-03-09', 33, 0.00, 110, NULL, 0, 1, 2, '2016-05-20', NULL),
(118, '2016-03-09', 57, 0.00, 111, NULL, 0, 1, 2, '2016-07-28', NULL),
(119, '2016-03-09', 440, 0.00, 112, NULL, 0, 1, 2, '2016-10-05', NULL),
(120, '2016-03-09', 12, 0.00, 113, NULL, 0, 1, 2, '2016-07-28', NULL),
(121, '2016-03-09', 154, 0.00, 114, NULL, 0, 1, 2, '2016-05-05', NULL),
(122, '2016-03-09', 1, 0.00, 117, NULL, 0, 1, 2, '2016-10-20', NULL),
(123, '2016-03-09', 7, 0.00, 118, NULL, 0, 1, 2, '2016-08-07', NULL),
(124, '2016-03-09', 8, 0.00, 119, NULL, 0, 1, 2, '2016-08-03', NULL),
(125, '2016-03-09', 48, 0.00, 120, NULL, 0, 1, 2, '2016-07-14', NULL),
(126, '2016-03-09', 48, 0.00, 121, NULL, 0, 1, 2, '2016-06-13', NULL),
(127, '2016-03-09', 17, 0.00, 122, NULL, 0, 1, 2, '2016-09-03', NULL),
(128, '2016-03-09', 208, 0.00, 124, NULL, 0, 1, 2, '2017-08-16', NULL),
(129, '2016-03-09', 40, 0.00, 125, NULL, 0, 1, 2, '2017-02-10', NULL),
(130, '2016-03-09', 24, 0.00, 126, NULL, 0, 1, 2, '2016-05-06', NULL),
(131, '2016-03-09', 240, 0.00, 127, NULL, 0, 1, 2, '2016-12-22', NULL),
(132, '2016-03-09', 72, 0.00, 128, NULL, 0, 1, 2, '2016-05-01', NULL),
(133, '2016-03-09', 90, 0.00, 129, NULL, 0, 1, 2, '2017-01-01', NULL),
(134, '2016-03-09', 24, 0.00, 131, NULL, 0, 1, 2, '2016-06-08', NULL),
(135, '2016-03-09', 72, 0.00, 132, NULL, 0, 1, 2, '2016-07-29', NULL),
(136, '2016-03-09', 90, 0.00, 133, NULL, 0, 1, 2, '2016-11-01', NULL),
(137, '2016-03-09', 216, 0.00, 170, NULL, 0, 1, 2, '2016-07-25', NULL),
(138, '2016-03-09', 108, 0.00, 135, NULL, 0, 1, 2, '2017-02-01', NULL),
(139, '2016-03-09', 72, 0.00, 171, NULL, 0, 1, 2, '2016-04-28', NULL),
(140, '2016-03-09', 7, 0.00, 92, NULL, 1, 5, 5, '2016-03-09', NULL),
(141, '2016-03-09', 9, 0.00, 88, NULL, 1, 5, 5, '2016-03-09', NULL),
(142, '2016-03-09', 8, 0.00, 89, NULL, 1, 5, 5, '2016-03-09', NULL),
(143, '2016-03-09', 12, 0.00, 189, NULL, 0, 1, 2, '2016-12-01', NULL),
(144, '2016-03-09', 16, 0.00, 190, NULL, 0, 1, 2, '2016-12-01', NULL),
(145, '2016-03-09', 24, 0.00, 192, NULL, 0, 1, 2, '2017-06-01', NULL),
(146, '2016-03-09', 12, 0.00, 193, NULL, 0, 1, 2, '2017-01-01', NULL),
(147, '2016-03-09', 6, 0.00, 194, NULL, 0, 1, 2, '2017-01-01', NULL),
(148, '2016-03-09', 6, 0.00, 195, NULL, 0, 1, 2, '2017-02-02', NULL),
(149, '2016-03-09', 6, 0.00, 196, NULL, 0, 1, 2, '2017-02-10', NULL),
(150, '2016-03-09', 14, 0.00, 197, NULL, 0, 1, 2, '2017-04-02', NULL),
(151, '2016-03-09', 12, 0.00, 198, NULL, 0, 1, 2, '2017-01-01', NULL),
(152, '2016-03-09', 90, 0.00, 91, NULL, 0, 1, 2, '2016-07-27', NULL),
(153, '2016-03-09', 135, 0.00, 92, NULL, 0, 1, 2, '2016-07-20', NULL),
(154, '2016-03-09', 45, 0.00, 93, NULL, 0, 1, 2, '2016-05-29', NULL),
(155, '2016-03-09', 90, 0.00, 87, NULL, 0, 1, 2, '2016-08-16', NULL),
(156, '2016-03-09', 178, 0.00, 90, NULL, 0, 1, 2, '2016-07-24', NULL),
(157, '2016-03-10', 200, 0.00, 202, NULL, 0, 1, 2, '2016-07-03', NULL),
(158, '2016-03-10', 360, 0.00, 201, NULL, 0, 1, 2, '2016-09-05', NULL),
(159, '2016-03-10', 240, 0.00, 200, NULL, 0, 1, 2, '2016-10-24', NULL),
(160, '2016-03-08', 10, 0.00, 105, 1, 1, 4, 3, '2016-03-10', NULL),
(161, '2016-03-10', 6, 0.00, 108, 1, 1, 4, 3, '2016-03-10', NULL),
(162, '2016-03-10', 10, 0.00, 114, 1, 1, 4, 3, '2016-03-10', NULL),
(163, '2016-03-10', 10, 0.00, 46, 1, 1, 4, 3, '2016-03-10', NULL),
(164, '2016-03-10', 15, 0.00, 41, 1, 1, 4, 3, '2016-03-10', NULL),
(165, '2016-03-10', 15, 0.00, 80, 1, 1, 4, 3, '2016-03-10', NULL),
(166, '2016-03-10', 12, 0.00, 85, 1, 1, 4, 3, '2016-03-10', NULL),
(167, '2016-03-10', 18, 0.00, 135, 1, 1, 4, 3, '2016-03-10', NULL),
(168, '2016-03-10', 24, 0.00, 132, 1, 1, 4, 3, '2016-03-10', NULL),
(170, '2016-03-10', 10, 0.00, 114, 2, 1, 6, 4, '2016-03-10', NULL),
(171, '2016-03-10', 12, 0.00, 30, 2, 1, 6, 4, '2016-03-10', NULL),
(172, '2016-03-10', 12, 0.00, 35, 2, 1, 6, 4, '2016-03-10', NULL),
(173, '2016-03-10', 12, 0.00, 8, 2, 1, 6, 4, '2016-03-10', NULL),
(174, '2016-03-10', 23, 0.00, 105, 2, 1, 6, 4, '2016-03-10', NULL),
(175, '2016-03-10', 16, 0.00, 37, 2, 1, 6, 4, '2016-03-10', NULL),
(176, '2016-03-10', 10, 0.00, 152, 2, 1, 6, 4, '2016-03-10', NULL),
(177, '2016-03-10', 10, 0.00, 150, 2, 1, 6, 4, '2016-03-10', NULL),
(178, '2016-03-10', 10, 0.00, 98, 2, 1, 6, 4, '2016-03-10', NULL),
(179, '2016-03-22', 10, 0.00, 4, 4, 1, 3, 1, NULL, NULL),
(180, '2016-03-27', 7, 45.00, 4, NULL, 2, 1, 2, NULL, 3),
(181, '2016-03-27', 10, 57.00, 84, NULL, 2, 1, 2, NULL, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `numeracion`
--

CREATE TABLE `numeracion` (
`IdNumeracion` int(10) NOT NULL,
  `TipoDocumento` varchar(50) NOT NULL,
  `Numeracion` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `numeracion`
--

INSERT INTO `numeracion` (`IdNumeracion`, `TipoDocumento`, `Numeracion`) VALUES
(1, 'remito', 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
`IdProducto` int(10) NOT NULL,
  `IdProveedor` int(10) NOT NULL,
  `IdMarca` int(10) NOT NULL,
  `IdTipoProducto` int(11) NOT NULL,
  `IdTipoPresentacion` int(4) NOT NULL,
  `CodBarra` varchar(50) NOT NULL,
  `Descripcion` varchar(150) NOT NULL,
  `CantPresentacion` int(4) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=203 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`IdProducto`, `IdProveedor`, `IdMarca`, `IdTipoProducto`, `IdTipoPresentacion`, `CodBarra`, `Descripcion`, `CantPresentacion`) VALUES
(3, 4, 1, 4, 2, '7790580317607', 'Topline 7  Xplosive Mint', 16),
(4, 4, 1, 4, 2, '7790580361204', 'Topline 7  X-Bombs', 14),
(5, 4, 1, 4, 2, '7790580004279', 'top line cleanmint', 12),
(6, 4, 1, 5, 1, '7790040931206', 'sonrisas x 118gr', 1),
(7, 7, 5, 5, 1, '7790310002995', 'twistos x 45gr', 1),
(8, 4, 4, 5, 1, '7790040930209', 'Mellizas x 112 gr', 1),
(9, 7, 7, 5, 1, '7792170001262', 'Galleta Avena Quacker x 50 gr', 1),
(11, 4, 9, 5, 1, '7790040055414', 'Ser salvado x 145gr', 1),
(13, 12, 8, 5, 1, '7791787000736', 'Frolitas Trio x 160gr', 1),
(14, 4, 1, 4, 2, '7790580002435', 'top line greenmint', 12),
(15, 4, 1, 4, 2, '7790580004262', 'top line quasarmint', 12),
(16, 4, 12, 8, 2, '7790580468309', 'menthoplus zero pomelo rosado', 12),
(17, 4, 12, 8, 2, '7790580468200', 'menthoplus zero mentol', 12),
(18, 4, 12, 8, 2, '7790580242602', 'menthoplus acidas naranja', 12),
(19, 4, 12, 8, 2, '7790580217006', 'menthoplus acidas cereza', 12),
(20, 4, 12, 8, 2, '7790580101077', 'menthoplus creamix berries 27.2gr', 12),
(21, 4, 12, 8, 2, '7790580175108', 'menthoplus mentol', 12),
(22, 4, 12, 8, 2, '7790580175405', 'menthoplus mint', 12),
(23, 4, 12, 8, 2, '7790580175306', 'menthoplus honey', 12),
(24, 4, 12, 8, 2, '7790580175207', 'menthoplus strong', 12),
(25, 4, 1, 4, 2, '7790580511104', 'top line 7 strawberry', 16),
(27, 4, 13, 5, 1, '7790040714205', 'cereal mix avena y mza x 180gr', 1),
(29, 4, 14, 4, 1, '77940131', 'turron x 25gr', 1),
(30, 4, 4, 5, 1, '7790040930407', 'amor x 112gr', 1),
(31, 4, 14, 5, 1, '7790040566903', 'hogareñas mix de cer. x 176gr', 1),
(32, 3, 15, 5, 1, '7792200000234', '9 de oro light x 180gr', 1),
(33, 4, 4, 5, 1, '77903518', 'Opera x 92gr', 1),
(34, 4, 4, 5, 1, '7790040936102', 'Opera triple frutilla x 75gr', 1),
(35, 4, 4, 5, 1, '7790040930506', 'Rumba x 112gr', 1),
(36, 4, 14, 5, 1, '7790040569706', 'Hogareñas 7 semillas x 181gr', 1),
(37, 8, 16, 5, 1, '7622300829643', 'Lincoln x 153gr', 1),
(38, 4, 4, 5, 1, '7790040377707', 'Criollitas original x 100gr', 1),
(39, 4, 14, 5, 1, '7790040725003', 'Serranitas x 105gr', 1),
(40, 4, 4, 5, 1, '7790040929906', 'Chocolinas x 170gr', 1),
(41, 4, 4, 1, 1, '7790040953703', 'Tofi triple bco x 73gr', 1),
(42, 4, 4, 1, 1, '7790040996403', 'Bagley simple bco x 50gr', 1),
(43, 4, 4, 1, 1, '7790040996304', 'Bagley simple ngr x 50gr', 1),
(44, 4, 14, 1, 1, '7790040613706', 'Simple bon o bon ngr x 40gr', 1),
(45, 8, 1, 1, 1, '7622300817725', 'Suchard x 50gr', 1),
(46, 8, 16, 5, 2, '7622300398545', 'rococo x 160gr', 1),
(48, 4, 4, 5, 2, '7790040545939', 'criollitas 3cer x 140gr', 1),
(49, 4, 4, 5, 2, '7790040107540', 'Finitas x 125gr', 1),
(50, 2, 19, 12, 1, '7790387119206', 'recarga taragui', 10),
(51, 8, 20, 4, 1, '7791249448137', 'beldent sens mza/anana x 16.8gr', 12),
(52, 8, 20, 4, 1, '7622300854201', 'beldent inf berry x 25.2gr', 12),
(53, 2, 21, 8, 1, '7895144610948', 'mentos mza x 29.7gr', 12),
(54, 2, 21, 8, 1, '7895144691541', 'mentos tutti frutti x 29.7gr', 12),
(55, 2, 21, 8, 1, '7895144619767', 'mentos frutos rojos x 29.7gr', 12),
(56, 4, 12, 8, 1, '7790580468002', 'menthoplus zero cherry x 26.64gr', 12),
(57, 4, 12, 8, 1, '7790580732400', 'menthoplus zero limon x 26.64gr', 12),
(58, 4, 12, 8, 1, '7790580468101', 'menthoplus zero dzno x 26.64gr', 12),
(59, 4, 1, 4, 1, '7790580109189', 'topline 7 sandia x 14gr', 16),
(60, 4, 22, 4, 1, '7790580100070', 'citos choc/leche x 40gr', 14),
(61, 4, 22, 4, 1, '7790580100063', 'citos bombon x 40gr', 14),
(62, 4, 13, 2, 1, '7790040258204', 'cereal mix rell frut x 32gr', 18),
(63, 4, 13, 2, 1, '7790040351400', 'cereal mix vainilla x 28gr', 20),
(64, 4, 14, 4, 1, '7790580348304', 'mogul conf x 40gr', 12),
(65, 4, 22, 3, 1, '7790580105990', 'choc 3placeres x 55gr', 10),
(66, 4, 22, 3, 1, '7790580003531', 'choc bco & cookies x 55gr', 10),
(67, 4, 22, 3, 1, '7790580107727', 'choc butter toffees x 55gr', 10),
(68, 4, 22, 3, 1, '7790580531201', 'cofler block x 38gr', 20),
(69, 4, 22, 3, 1, '7790580003364', 'cofler leche x 27gr', 20),
(70, 4, 22, 3, 1, '7790580003395', 'cofler almendras x 27gr', 20),
(71, 4, 3, 3, 1, '7790580003692', 'aguila dor pasasx 72gr', 10),
(72, 4, 14, 3, 1, '7790580003678', 'aguila dor almendras x 72gr', 10),
(73, 4, 14, 3, 1, '7790580040536', 'aguila dor mousse x 72gr', 10),
(74, 8, 16, 4, 1, '7622300423759', 'rhodesia x 22gr', 36),
(75, 4, 23, 4, 1, '7790580618308', 'mogul frutales x 50gr', 12),
(76, 4, 22, 3, 1, '7790580103842', 'cofler exra mani x 38gr', 20),
(77, 4, 22, 3, 1, '7790580003753', 'cofler DDL x 42gr', 20),
(78, 4, 14, 4, 1, '7790580327408', 'rocklets x 40gr', 18),
(79, 4, 9, 1, 1, '7790040002203', 'ser arroz DDL x 23gr', 12),
(80, 8, 24, 1, 1, '77991584', 'fantoche triple bco x 85gr', 1),
(81, 4, 3, 1, 1, '7790040298804', 'minitorta brownie x 74gr', 1),
(82, 4, 22, 1, 1, '7790040659605', 'cofler block x 60gr', 1),
(83, 4, 3, 1, 1, '7790040298705', 'minitorta clasica x 72gr', 1),
(84, 4, 25, 1, 1, '77956699', 'tri shot x 60gr', 1),
(85, 4, 3, 1, 1, '7790040316508', 'minitorta blanca x 72gr', 1),
(86, 8, 24, 1, 1, '77991577', 'fantoche triple negro x 85gr', 1),
(87, 10, 26, 6, 2, '7790895000270', 'sprite x 354ml', 1),
(88, 10, 26, 6, 2, '7790895009693', 'aquarius pomelo x 354ml', 1),
(89, 10, 26, 6, 2, '7790895009549', 'aquarius multi x 354ml', 1),
(90, 10, 26, 6, 2, '7790895000232', 'coca cola x 354ml', 1),
(91, 10, 26, 6, 2, '7790895000256', 'fanta x 354ml', 1),
(92, 10, 26, 6, 2, '7790895004599', 'schweppes pomelo x 354ml', 1),
(93, 10, 26, 6, 2, '7790895067587', 'coca zero x 354ml', 1),
(94, 4, 12, 8, 1, '7790580101367', 'menthoplus c limon x 30.6gr', 12),
(95, 4, 12, 8, 1, '7790580282004', 'menthoplus c frutilla x 30.6gr', 12),
(96, 4, 12, 8, 1, '7790580101084', 'menthoplus creamix frutilla x 27.2gr', 12),
(97, 6, 10, 9, 2, '7792540250450', 'azucar ledesma x 1kg', 1),
(98, 8, 15, 5, 2, '7792200000128', '9 de oro agrid x 200gr', 1),
(99, 11, 27, 5, 2, '7798199770042', 'crisppino queso x 50gr', 1),
(100, 2, 7, 5, 2, '7790070411334', 'biscochos gallo dulces x 50gr', 1),
(101, 11, 27, 5, 2, '7798199770028', 'crisppino clasicas x 50gr', 1),
(102, 11, 27, 5, 2, '7798199770035', 'crisppino jamon x 50gr', 1),
(103, 12, 8, 5, 2, '7791787000422', 'pepas x 200gr', 1),
(104, 4, 7, 5, 2, '7790040102118', 'papas saladix x 36gr', 1),
(105, 2, 7, 5, 2, '7790263111003', 'okebon leche x 72gr', 1),
(106, 4, 14, 5, 2, '7790040455405', 'RDA bisc grasa x 200gr', 1),
(107, 8, 7, 5, 2, '7622300780814', 'cerealitas x 89gr', 1),
(108, 8, 7, 5, 2, '7622300834883', 'club social  orig x 43gr', 1),
(109, 4, 4, 5, 2, '7790040929807', 'coquitas x 170gr', 1),
(110, 2, 7, 12, 2, '7794619000324', 'mani x 200gr', 1),
(111, 8, 16, 5, 2, '7622300398569', 'anillos x 160gr', 1),
(112, 13, 28, 5, 2, '7798080660667', 'pepas valido x 150gr', 1),
(113, 8, 16, 5, 2, '7622300829728', 'melba x 120gr', 1),
(114, 12, 15, 5, 2, '7792200000159', '9 de oro grasa x 200gr', 1),
(115, 4, 4, 5, 2, '7790040103153', 'tentacion mousse frut x 113,5gr', 1),
(116, 4, 4, 5, 2, '7790040105812', 'rosquitas x 140gr', 1),
(117, 4, 13, 2, 1, '7790040954014', 'cereal mix frutilla x 28gr', 20),
(118, 4, 14, 2, 1, '7790040378605', 'cereal mix orig x 23gr', 20),
(119, 4, 13, 2, 1, '7790040258600', 'cereal mix rell durazno x 32gr', 18),
(120, 4, 14, 5, 2, '7790580387105', 'saladix  jamonx 30gr', 1),
(121, 4, 14, 5, 2, '7790580775407', 'saladix calabresa x 30gr', 1),
(122, 4, 4, 5, 2, '7790040003569', 'rex x 75gr', 1),
(123, 4, 14, 5, 2, '7790040003606', 'kesitas x 75gr', 1),
(124, 13, 28, 5, 2, '7798080660209', 'coco valido x 150gr', 1),
(125, 13, 28, 5, 2, '7798080660780', 'zaira valido x 150gr', 1),
(126, 8, 16, 5, 2, '7622300342142', 'cerealitas nar x 134gr', 1),
(127, 13, 28, 5, 2, '7798080660216', 'miel valido x 150gr', 1),
(128, 12, 7, 7, 2, '7790036006741', 'baggio DDL x 200ml', 1),
(129, 12, 7, 7, 2, '7790036000565', 'baggio nar x 200ml', 1),
(130, 4, 7, 7, 2, '7793360106095', 'BC naranja x 200ml', 1),
(131, 12, 7, 7, 2, '7794000792579', 'Ades dzno x 200ml', 1),
(132, 12, 7, 7, 2, '7794000792586', 'Ades futas trop x 200ml', 1),
(133, 12, 7, 7, 2, '7790036000572', 'baggio mza x 200ml', 1),
(134, 4, 7, 7, 2, '7793360106064', 'BC multi x 200ml', 1),
(135, 12, 7, 7, 2, '7790036000619', 'baggio multi x 200ml', 1),
(136, 2, 7, 9, 1, '7790150770443', 'toque de crema', 25),
(137, 2, 7, 9, 2, '7790150695968', 'TUY edulcorante 0,8gr', 1),
(138, 3, 7, 9, 2, '7613035098701', 'cafe soluble x 1kg', 1),
(139, 3, 7, 9, 2, '7613035092556', 'leche svelty x 1kg', 1),
(140, 3, 7, 9, 2, '7613035211995', 'chocolate x 1kg', 1),
(141, 3, 7, 9, 1, '00000000001', 'paletinas', 1000),
(142, 3, 7, 9, 1, '00000000005', 'vasos x tira', 100),
(143, 3, 7, 9, 2, '00000000004', 'mate cocido solumat x 1kg', 1),
(144, 3, 7, 9, 2, '00000000006', 'te c/limon T MONS x 1kg', 1),
(145, 3, 7, 9, 2, '00000000003', 'mono dosis cafe x 7gr', 1),
(146, 3, 7, 9, 2, '00000000002', 'azucar en sobre x 6,250gr', 1),
(147, 3, 7, 12, 2, '00000000007', 'opcional', 1),
(148, 3, 7, 9, 2, '7790150016268', 'cafe grano cartagena x 1 kg', 1),
(149, 3, 7, 5, 2, '7791324156926', 'suavecitas x 140gr', 1),
(150, 3, 7, 5, 2, '7791324157022', 'pitusa vainilla x 160gr', 1),
(151, 3, 7, 5, 2, '7791324000069', 'pitusa limon x 160gr', 1),
(152, 3, 7, 5, 2, '7791324000014', 'locuras yogurt/dzno x 140gr', 1),
(153, 3, 7, 5, 2, '7791324156995', 'pepitas x 160gr', 1),
(154, 3, 7, 5, 2, '7791324157466', 'pitusa frutilla x 160gr', 1),
(155, 3, 7, 9, 1, '7790150080115', 'mi cafe saquitos', 20),
(156, 4, 1, 4, 1, '7790580004286', 'top line happy mint x 24gr', 12),
(157, 8, 18, 8, 1, '7791249450277', 'halls menta x 28gr', 12),
(158, 8, 18, 8, 1, '7791249005101', 'halls miel x 28gr', 12),
(159, 8, 18, 8, 1, '7791249444825', 'halls cherry x 28gr', 12),
(160, 8, 18, 8, 1, '7791249450253', 'halls strong x 28gr', 12),
(161, 3, 7, 3, 1, '7798138290075', 'vauquita light x 22gr', 18),
(162, 4, 22, 3, 1, '7790580107826', 'cofler exra banana split x 30gr', 20),
(163, 7, 18, 2, 1, '7792170001088', 'quaker frutilla x 30gr', 15),
(164, 4, 7, 4, 1, '7792222002919', 'rueditas x 25gr', 16),
(165, 2, 15, 5, 2, '7792200000791', '9 de oro sconcitos x 200gr', 1),
(166, 4, 9, 1, 1, '7790040002210', 'ser arroz limon x 23gr', 12),
(167, 4, 3, 1, 2, '7790040298903', 'minitorta coco x 72gr', 1),
(168, 4, 14, 1, 2, '7790040405301', 'tatin blanco', 1),
(169, 4, 14, 1, 2, '7790040405608', 'tatin negro', 1),
(170, 12, 7, 7, 2, '7790036972602', 'baggio chocolatada x 200ml', 1),
(171, 12, 7, 7, 2, '7790036006666', 'baggio capuchino x 200ml', 1),
(172, 9, 7, 9, 1, '7790150100080', 'clasico cafe en sobres ', 36),
(173, 2, 7, 9, 2, '7794520000789', '5 hispano Cafe premium', 1),
(174, 2, 29, 2, 1, '7798146300544', 'barra de arroz yogurt frutilla', 20),
(175, 2, 29, 2, 1, '7798146300551', 'barra de arroz dulce de leche', 20),
(176, 2, 29, 2, 1, '7798146300520', 'barra de arroz chocolate', 20),
(177, 2, 29, 2, 1, '7798146300537', 'barra de arroz blanco', 20),
(178, 2, 5, 5, 2, '7790310004012', 'twistos crackers original', 1),
(179, 2, 7, 4, 1, '7798138290020', 'vauquita dulce de leche 25gr', 18),
(180, 2, 5, 5, 2, '7790310004180', 'Crackers horneados queso crema', 1),
(181, 2, 5, 5, 2, '7790310004036', 'Crackers horneados Original', 1),
(182, 2, 5, 5, 2, '7790310003008', 'minitostaditas cuatro quesos', 1),
(183, 2, 15, 5, 2, '7792200000777', '9 de oro light x 200gr', 1),
(184, 2, 15, 5, 2, '7792200020119', '9 de oro salvado x 200gr', 1),
(185, 2, 29, 5, 2, '7798146300308', 'tostaditas de arroz', 1),
(186, 2, 7, 5, 2, '7790310003251', 'pizza fugazzeta', 1),
(187, 2, 7, 5, 2, '7790310003626', 'pizza jamon', 1),
(188, 3, 7, 5, 2, '7622300833909', 'pepitos x 118gr', 1),
(189, 2, 7, 4, 1, '7790380005384', 'Chococereal s/azucar x 25gr', 12),
(190, 2, 7, 4, 1, '7790380417118', 'nucrem s/azucar x 35gr', 16),
(191, 2, 7, 4, 2, '7790380016922', 'nucrem x 84gr', 240),
(192, 2, 7, 3, 1, '7790380017400', 'ch. amargo x 30gr', 12),
(193, 2, 7, 2, 1, '7790380415022', 'Vitacereal c/frut y alm x 23gr', 20),
(194, 2, 7, 2, 1, '7790380420026', 'Vitacereal frutos del bosque x 23gr', 20),
(195, 2, 7, 2, 1, '7790380414926', 'flow cereal frutas x 23gr', 20),
(196, 2, 7, 2, 1, '7790380416197', 'flow cereal yogurt x 27gr', 20),
(197, 2, 7, 4, 1, '7790380416913', 'nucrem x 43gr', 16),
(198, 2, 7, 3, 1, '7790380045717', 'ch. s/azucar c/leche x 30gr', 12),
(199, 2, 7, 3, 1, '7790380424246', 'ch. s/azucar blanco x 30gr', 12),
(200, 12, 8, 5, 2, '7791787000682', 'tri choc x 180gr', 1),
(201, 3, 7, 5, 2, '7790628102684', 'vainilla x 80gr', 1),
(202, 12, 8, 5, 2, '7792200000319', '9 de oro azucarado x 200gr', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
`IdProveedor` int(10) NOT NULL,
  `RazonSocial` varchar(100) NOT NULL,
  `Cuit` varchar(100) NOT NULL,
  `Telefono` varchar(100) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`IdProveedor`, `RazonSocial`, `Cuit`, `Telefono`, `Direccion`, `Email`) VALUES
(2, 'Proveedor 1', '200000', '1200000', 'Talero', 'proveedor@gmail.com'),
(3, 'Nuevo Proveedor', '20000000', '200000000', 'Eduardo Talero', 'nuevo@gmail.com'),
(4, 'ILLANES SRL', '20245030267', '88888', 'xxxx', 'contacto@gmail.com'),
(5, 'juancho', '2222', '22', '2', 'ss'),
(6, 'Organizacion Comercial Don Tomas SRL', '30570847631', '(0298) 4435440', 'Chula Vista 831. Gral Roca', 'dontomassrl@dontomassrl.com.ar'),
(7, 'Pepsico', '111', '111', 'mkfksk', '@ggogo'),
(8, 'logistica 1914', '9393939', '49493394', 'jdkfdlfjf', 'ssdsd@dddf'),
(9, 'proveedor', '2033545', '201545+4+', 'algun lugar', 'algo@pacifica.com'),
(10, 'embotelladora', '53543', '5332', 'alhunaa', 'vdgdg@gmail.com'),
(11, 'lomas', '544787+', '45474', '4789+789+7+', 'munckhansen@gmail.com'),
(12, 'el trio', '1854215', '574+216+7', 'gbufgf', 'hftfdt'),
(13, 'valido', '465444', '565648', 'djkhfsdjkohf', 'ghdffg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE `provincia` (
  `IdProvincia` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`IdProvincia`, `Nombre`) VALUES
(1, 'Buenos Aires'),
(2, 'Catamarca'),
(3, 'Chaco'),
(4, 'Chubut'),
(5, 'Córdoba'),
(6, 'Corrientes'),
(7, 'Entre Ríos'),
(8, 'Formosa'),
(9, 'Jujuy'),
(10, 'La Pampa'),
(11, 'La Rioja'),
(12, 'Mendoza'),
(13, 'Misiones'),
(14, 'Neuquén'),
(15, 'Río Negro'),
(16, 'Salta'),
(17, 'San Juan'),
(18, 'San Luis'),
(19, 'Santa Cruz'),
(20, 'Santa Fe'),
(21, 'Santiago del Estero'),
(22, 'Tierra del Fuego'),
(23, 'Tucumán');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `remito`
--

CREATE TABLE `remito` (
`IdRemito` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `IdDestino` int(10) NOT NULL,
  `IdUsuario` int(11) NOT NULL,
  `Numero` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `remito`
--

INSERT INTO `remito` (`IdRemito`, `Fecha`, `IdDestino`, `IdUsuario`, `Numero`) VALUES
(2, '2016-03-08', 6, 4, '0001-00000002'),
(3, '2016-03-09', 5, 5, '0001-00000003'),
(4, '2016-03-22', 5, 1, '0001-00000004');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
`IdRol` int(10) unsigned NOT NULL,
  `Nombre` varchar(20) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`IdRol`, `Nombre`) VALUES
(1, 'Administrador'),
(2, 'Deposito'),
(3, 'Gerente'),
(4, 'Operador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rolaccion`
--

CREATE TABLE `rolaccion` (
  `IdRol` int(10) unsigned NOT NULL,
  `IdAccion` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `rolaccion`
--

INSERT INTO `rolaccion` (`IdRol`, `IdAccion`) VALUES
(1, 1),
(2, 1),
(4, 1),
(1, 2),
(2, 2),
(4, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(2, 22),
(1, 23),
(2, 23),
(1, 24),
(2, 24),
(1, 25),
(2, 25),
(1, 26),
(2, 26),
(1, 27),
(2, 27),
(1, 28),
(2, 28),
(1, 29),
(2, 29),
(1, 30),
(2, 30),
(1, 31),
(2, 31),
(1, 32),
(2, 32),
(1, 33),
(2, 33),
(1, 34),
(2, 34),
(2, 36),
(4, 36),
(2, 37),
(2, 38),
(2, 39),
(2, 40),
(2, 41),
(2, 42);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipopresentacion`
--

CREATE TABLE `tipopresentacion` (
`IdTipoPresentacion` int(10) NOT NULL,
  `Descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipopresentacion`
--

INSERT INTO `tipopresentacion` (`IdTipoPresentacion`, `Descripcion`) VALUES
(1, 'Blister'),
(2, 'Unidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipoproducto`
--

CREATE TABLE `tipoproducto` (
`IdTipoProducto` int(10) NOT NULL,
  `TipoProducto` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipoproducto`
--

INSERT INTO `tipoproducto` (`IdTipoProducto`, `TipoProducto`) VALUES
(1, 'Alfajor'),
(2, 'Cereal'),
(3, 'Chocolate'),
(4, 'Golosina'),
(5, 'Galletita'),
(6, 'Gaseosa'),
(7, 'Jugo'),
(8, 'Pastillas'),
(9, 'Maquina'),
(11, 'Yogur'),
(12, 'otro prod');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
`IdUsuario` int(10) unsigned NOT NULL,
  `Apellido` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `Nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `Foto` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `Username` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `Password` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `IdRol` int(10) unsigned NOT NULL,
  `Activo` int(1) unsigned NOT NULL,
  `IdDestino` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`IdUsuario`, `Apellido`, `Nombre`, `Foto`, `Username`, `Password`, `IdRol`, `Activo`, `IdDestino`) VALUES
(1, 'Admin', 'Admin', NULL, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1, 0),
(2, 'deposito', 'deposito', NULL, 'deposito', 'caaf856169610904e4f188e6ee23e88c', 2, 1, 0),
(3, 'arias', 'sergio', NULL, 'ruta15', 'e10adc3949ba59abbe56e057f20f883e', 4, 1, 4),
(4, 'juan jose', 'vivanco', NULL, 'ruta51', 'e10adc3949ba59abbe56e057f20f883e', 4, 1, 6),
(5, 'palacios', 'esteban', NULL, 'ruta22', 'e10adc3949ba59abbe56e057f20f883e', 4, 1, 5),
(6, 'cabrera', 'martin', NULL, 'ruta4', 'e10adc3949ba59abbe56e057f20f883e', 4, 1, 3),
(7, 'lahora', 'matias', NULL, 'ruta3', 'e10adc3949ba59abbe56e057f20f883e', 4, 1, 2),
(8, 'lavin', 'juan carlos', NULL, 'tecnica', 'e10adc3949ba59abbe56e057f20f883e', 4, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuariologin`
--

CREATE TABLE `usuariologin` (
`IdUsuarioLogin` int(10) unsigned NOT NULL,
  `IdUsuario` int(10) unsigned NOT NULL,
  `Ip` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `SessionId` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `FechaHoraLogin` datetime NOT NULL,
  `FechaHoraLogout` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuariologin`
--

INSERT INTO `usuariologin` (`IdUsuarioLogin`, `IdUsuario`, `Ip`, `SessionId`, `FechaHoraLogin`, `FechaHoraLogout`) VALUES
(1, 2, '192.168.0.26', '07om3tuhah9in45mmogl82bgv7', '2016-03-04 13:11:08', NULL),
(2, 3, '192.168.0.26', 'jesegknj8t3p0j2e3uml8i2ll7', '2016-03-04 13:13:55', NULL),
(3, 2, '192.168.0.26', 'lpkh9ala2b5jes74tl5cb96896', '2016-03-04 13:18:17', '2016-03-04 13:25:53'),
(4, 1, '192.168.0.26', '3c2dqvh5bi1u0iu022j1jsb0k4', '2016-03-04 13:26:09', '2016-03-04 13:27:28'),
(5, 3, '192.168.0.26', 'gjsd79apun74t09g8add7cv171', '2016-03-04 13:27:36', '2016-03-04 13:28:55'),
(6, 2, '192.168.0.26', '577n39iv6mspvpkbfo4uiibsa4', '2016-03-04 13:32:01', NULL),
(7, 2, '192.168.0.26', 'lj7mlkkqr8318rnk02re1v5ef3', '2016-03-04 15:47:40', NULL),
(8, 3, '192.168.0.26', 'sbtmvfl46b6cvp7pnnv33oltq3', '2016-03-04 16:03:28', '2016-03-04 16:07:56'),
(9, 2, '192.168.0.26', 'tk5mqhqsqvo5v6t13bueq1eot4', '2016-03-04 16:09:25', '2016-03-04 16:12:42'),
(10, 1, '192.168.0.26', '6d3a9b2g1685abisu482vf97n7', '2016-03-04 16:12:50', '2016-03-04 16:13:05'),
(11, 2, '192.168.0.26', 'og32v1cli6vs3idj744un5vb43', '2016-03-04 16:26:27', '2016-03-04 18:05:44'),
(12, 2, '192.168.0.26', 'e9ia1nedhrci6q0agjqhm47987', '2016-03-04 20:11:44', '2016-03-04 20:19:36'),
(13, 2, '192.168.0.26', 'hnfhj8nct7lu84l4jnehdphi14', '2016-03-05 13:27:12', NULL),
(14, 3, '192.168.0.26', 'p4rcif1l3f1hsb4f2n9ff48a54', '2016-03-07 12:03:30', '2016-03-07 13:04:14'),
(15, 2, '192.168.0.26', 'ash8lg19mq7dukf6lf9hbs3326', '2016-03-07 13:04:23', '2016-03-07 14:21:24'),
(16, 1, '192.168.0.26', 'u87c1rf32uf21r18gb4di4rd12', '2016-03-07 14:21:33', '2016-03-07 14:26:26'),
(17, 1, '192.168.0.26', 'cch2cbck4udq809u4u6548j252', '2016-03-07 14:26:37', '2016-03-07 14:28:29'),
(18, 8, '192.168.0.26', 'tr03udnlhkabrm0hai8ri2idg7', '2016-03-07 14:28:40', '2016-03-07 14:28:47'),
(19, 3, '192.168.0.26', '7cg040b68g2v5fbedlfob3k843', '2016-03-07 14:28:59', '2016-03-07 14:32:41'),
(20, 2, '192.168.0.26', '8vgup8geubkt3fh2mom7rgot11', '2016-03-07 14:32:49', '2016-03-07 14:41:53'),
(21, 4, '192.168.0.26', 'liesovo2o13js8f87hsjhal8h4', '2016-03-07 14:42:05', '2016-03-07 15:06:13'),
(22, 2, '192.168.0.26', 'mb3okd4d1orf0nl0f22vs5vgk1', '2016-03-07 15:06:21', '2016-03-07 15:10:42'),
(23, 4, '192.168.0.26', 'rls27ouuou4nil8mevv6j1jb62', '2016-03-07 15:10:52', '2016-03-07 15:16:52'),
(24, 2, '192.168.0.26', 't1eccbcdvk3b4uoq7ei8i1hfo6', '2016-03-07 15:17:00', '2016-03-07 15:17:31'),
(25, 4, '192.168.0.26', 'aol33845s5790t5j9hmt28ug32', '2016-03-07 15:17:40', '2016-03-07 15:19:13'),
(26, 2, '192.168.0.26', 'aaq0il2419jgveffistm22eu01', '2016-03-07 15:19:20', '2016-03-07 15:20:46'),
(27, 4, '192.168.0.26', '4mj8jrjmu1p0l86t70k44n9lc1', '2016-03-07 15:20:54', '2016-03-07 15:22:48'),
(28, 2, '192.168.0.26', '1q980d1i5q88klvrtbklpoi3p5', '2016-03-07 15:22:58', '2016-03-07 15:24:16'),
(29, 1, '192.168.0.26', '240ttsi55ggnhv64k0dv7tjrq3', '2016-03-07 15:24:28', '2016-03-07 15:25:20'),
(30, 4, '192.168.0.26', 'rhc192fuirsoqdec938rol2io4', '2016-03-07 15:25:34', '2016-03-07 15:30:15'),
(31, 1, '192.168.0.26', 'gs9kserpt38t8695aanhkjjme5', '2016-03-07 15:31:48', '2016-03-07 15:33:03'),
(32, 4, '192.168.0.26', 'mbqbcspr594bmkr15vtmp3p1a7', '2016-03-07 15:33:19', '2016-03-07 15:56:33'),
(33, 2, '192.168.0.26', 'l37vecbor9eg0nschv3b1p03h1', '2016-03-07 15:56:54', '2016-03-07 16:21:41'),
(34, 2, '192.168.0.26', 'abnh071nsequjg6oov8avu6sl3', '2016-03-07 16:26:47', '2016-03-07 16:34:14'),
(35, 4, '192.168.0.26', 'vmrpl5lpclkudurb5bui8kfos0', '2016-03-07 16:34:45', '2016-03-07 17:04:15'),
(36, 2, '192.168.0.26', 's9u46ak3pl7r1ch9jbg5v9q6i3', '2016-03-07 17:04:22', NULL),
(37, 2, '192.168.0.26', '93j5cvrtmv7aask9uk4npl7c44', '2016-03-08 12:22:16', NULL),
(38, 2, '192.168.0.26', '8nqk631mh2np7p6g85n4hfmem1', '2016-03-08 19:24:08', '2016-03-08 19:29:41'),
(39, 2, '192.168.0.26', 'fg009cvcko9sdv3g6cmsegu1j7', '2016-03-08 19:33:00', NULL),
(40, 2, '192.168.0.26', '821upnapqvu8obbipssetfd2p3', '2016-03-08 21:48:33', '2016-03-09 12:56:25'),
(41, 4, '192.168.0.26', '7s91ja6efo22ss9mtcvmu0rp92', '2016-03-09 12:57:10', '2016-03-09 13:00:03'),
(42, 2, '192.168.0.26', 'pj9gm15rabfgoi7lg3vq3k6m45', '2016-03-09 13:00:14', '2016-03-09 14:26:19'),
(43, 5, '192.168.0.26', '2bq1p28pj3j87p2sceoktqea36', '2016-03-09 14:26:32', '2016-03-09 15:18:35'),
(44, 2, '192.168.0.26', 'ghpbq0euajodaq4ulp56dm0o76', '2016-03-09 16:39:49', NULL),
(45, 2, '192.168.0.26', '8rlqv9kl0gk10ldea47rajhag0', '2016-03-09 18:08:24', '2016-03-09 18:33:48'),
(46, 2, '192.168.0.26', '30b19nlk3hc6d59s0qn2vhf0s4', '2016-03-10 13:14:44', '2016-03-10 13:15:47'),
(47, 4, '192.168.0.26', 'hkdleplpq42vnbahvb4vuq0a93', '2016-03-10 13:15:55', NULL),
(48, 2, '192.168.0.26', 'c5ptm6h82n2krrgdmtkd0qcdm5', '2016-03-10 17:49:34', '2016-03-10 18:02:03'),
(49, 2, '192.168.0.26', 'ke7ufibse6url33furmjf3nmt3', '2016-03-10 18:45:21', '2016-03-22 03:49:34'),
(50, 1, '::1', 'e9c401d576e323fb4ab6ce0063795649', '2016-03-22 03:49:39', '2016-03-22 04:20:35'),
(51, 2, '::1', 'ebc981210b5f340b816cedc52c299c3c', '2016-03-22 04:20:40', '2016-03-26 23:37:00'),
(52, 1, '::1', '96420332b54e4bbcb560f7b6a7e29189', '2016-03-26 23:37:12', '2016-03-26 23:41:05'),
(53, 2, '::1', '10ce54648e5cf00645bf8061c91f472d', '2016-03-26 23:41:07', '2016-03-26 23:48:12'),
(54, 2, '::1', 'a4f5fa6202d1b4a00e035e0a646109d6', '2016-03-26 23:48:15', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `accion`
--
ALTER TABLE `accion`
 ADD PRIMARY KEY (`IdAccion`);

--
-- Indices de la tabla `auditoria`
--
ALTER TABLE `auditoria`
 ADD PRIMARY KEY (`IdAuditoria`), ADD KEY `fk_auditoria_usuario` (`IdUsuario`);

--
-- Indices de la tabla `destino`
--
ALTER TABLE `destino`
 ADD PRIMARY KEY (`IdDestino`);

--
-- Indices de la tabla `existencia`
--
ALTER TABLE `existencia`
 ADD PRIMARY KEY (`IdExistencia`);

--
-- Indices de la tabla `localidad`
--
ALTER TABLE `localidad`
 ADD PRIMARY KEY (`IdLocalidad`), ADD KEY `fk_localidad_provincia1_idx` (`IdProvincia`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
 ADD PRIMARY KEY (`IdMarca`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
 ADD PRIMARY KEY (`IdMenu`), ADD KEY `fk_menu_accion` (`IdAccion`), ADD KEY `fk_menu_menu` (`IdMenuPadre`);

--
-- Indices de la tabla `motivo`
--
ALTER TABLE `motivo`
 ADD PRIMARY KEY (`IdMotivo`);

--
-- Indices de la tabla `movimientoproducto`
--
ALTER TABLE `movimientoproducto`
 ADD PRIMARY KEY (`IdMovimiento`), ADD KEY `fk_movimientoproducto_producto` (`IdProducto`), ADD KEY `fk_movimientoproducto_destino` (`IdDestino`);

--
-- Indices de la tabla `numeracion`
--
ALTER TABLE `numeracion`
 ADD PRIMARY KEY (`IdNumeracion`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
 ADD PRIMARY KEY (`IdProducto`), ADD KEY `fk_producto_proveedor` (`IdProveedor`), ADD KEY `fk_producto_marca` (`IdMarca`), ADD KEY `fk_producto_tipoproducto` (`IdTipoProducto`), ADD KEY `fk_producto_tipopresentacion` (`IdTipoPresentacion`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
 ADD PRIMARY KEY (`IdProveedor`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
 ADD PRIMARY KEY (`IdProvincia`);

--
-- Indices de la tabla `remito`
--
ALTER TABLE `remito`
 ADD PRIMARY KEY (`IdRemito`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
 ADD PRIMARY KEY (`IdRol`);

--
-- Indices de la tabla `rolaccion`
--
ALTER TABLE `rolaccion`
 ADD PRIMARY KEY (`IdRol`,`IdAccion`), ADD KEY `fk_rolaccion_accion` (`IdAccion`);

--
-- Indices de la tabla `tipopresentacion`
--
ALTER TABLE `tipopresentacion`
 ADD PRIMARY KEY (`IdTipoPresentacion`);

--
-- Indices de la tabla `tipoproducto`
--
ALTER TABLE `tipoproducto`
 ADD PRIMARY KEY (`IdTipoProducto`), ADD UNIQUE KEY `IdTipoProducto` (`IdTipoProducto`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`IdUsuario`), ADD KEY `fk_usuario_rol` (`IdRol`);

--
-- Indices de la tabla `usuariologin`
--
ALTER TABLE `usuariologin`
 ADD PRIMARY KEY (`IdUsuarioLogin`), ADD KEY `fk_usuariologin_usuario` (`IdUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `accion`
--
ALTER TABLE `accion`
MODIFY `IdAccion` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT de la tabla `auditoria`
--
ALTER TABLE `auditoria`
MODIFY `IdAuditoria` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=60;
--
-- AUTO_INCREMENT de la tabla `destino`
--
ALTER TABLE `destino`
MODIFY `IdDestino` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `existencia`
--
ALTER TABLE `existencia`
MODIFY `IdExistencia` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=142;
--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
MODIFY `IdMarca` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `menu`
--
ALTER TABLE `menu`
MODIFY `IdMenu` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `motivo`
--
ALTER TABLE `motivo`
MODIFY `IdMotivo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `movimientoproducto`
--
ALTER TABLE `movimientoproducto`
MODIFY `IdMovimiento` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=182;
--
-- AUTO_INCREMENT de la tabla `numeracion`
--
ALTER TABLE `numeracion`
MODIFY `IdNumeracion` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
MODIFY `IdProducto` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=203;
--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
MODIFY `IdProveedor` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `remito`
--
ALTER TABLE `remito`
MODIFY `IdRemito` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
MODIFY `IdRol` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tipopresentacion`
--
ALTER TABLE `tipopresentacion`
MODIFY `IdTipoPresentacion` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tipoproducto`
--
ALTER TABLE `tipoproducto`
MODIFY `IdTipoProducto` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
MODIFY `IdUsuario` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `usuariologin`
--
ALTER TABLE `usuariologin`
MODIFY `IdUsuarioLogin` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;