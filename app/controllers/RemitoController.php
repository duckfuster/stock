<?php

namespace app\controllers;

use Yii;
use app\models\Remito;
use app\models\RemitoSearch;
use app\models\Usuario;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\vendor\base\BaseController;
use app\models\Rol;
use app\models\Numeracion;

/**
 * RemitoController implements the CRUD actions for Remito model.
 */
class RemitoController extends BaseController
{
    /**
     * Lists all Remito models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RemitoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Remito model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Remito model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        // si el usuario es deposito voy al formulario sino lo crea automaticamente
        
        
        $model = new Remito();
        $model->Fecha = date("Y-m-d");
        
        $idUser=Yii::$app->user->getId();
        $user = Usuario::findOne($idUser);  
        
        
        $prefijo='0001';
        
        $numeracion= Numeracion::find()->where(['TipoDocumento' => 'remito'])->one();
        $nro_remito=$numeracion->Numeracion;
     
        $posfijo = sprintf("%08s",$nro_remito);
        $model->Numero=$prefijo.'-'.$posfijo;
        
        
        if ($user->IdRol==Rol::DEPOSITO){
             if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('success', 'El registro ha sido guardado!');
                $nro_remito=$nro_remito+1;
                $numeracion->Numeracion=$nro_remito;
                $numeracion->Save();
            return $this->redirect(['index']);
             } else {
                return $this->render('create', [
                    'model' => $model,
                    ]);
            }
        }else{
             $model->IdUsuario=$idUser;
             $model->IdDestino = $user->IdDestino;
             $model->save();
             
             $nro_remito=$nro_remito+1;
             $numeracion->Numeracion=$nro_remito;
             $numeracion->Save();
             
              Yii::$app->session->setFlash('success', 'El registro ha sido guardado!');
            return $this->redirect(['remito/index']);
        }
        
    }

    /**
     * Updates an existing Remito model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->IdRemito]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Remito model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Remito model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Remito the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Remito::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
