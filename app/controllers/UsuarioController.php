<?php

namespace app\controllers;

use Yii;
use app\models\Usuario;
use app\models\UsuarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\vendor\base\BaseController;
use yii\web\UploadedFile;

/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends BaseController
{

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Usuario();
        $model->Activo = 1;
        $oldPassword = "";

        if ($model->load(Yii::$app->request->post())){
            $oldPassword = $model->Password;
            $model->Password = md5($model->Password);
            $model->Foto = UploadedFile::getInstance($model, 'Foto');

            if ($model->Foto) {                
                $imageName = 'upload/usuario/'.date('YmdHis').'_'.$model->Foto;
                $model->Foto->saveAs($imageName);
                $model->Foto = $imageName;
            }

            if($model->save()){
                return $this->redirect(['index']);
            }            
                    
        } 

        $model->Password = $oldPassword;
        return $this->render('create', ['model' => $model]);
    }
    
    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate2($id = '')
    {
        $model = new Usuario();
        $model->Activo = 1;
        $oldPassword = "";

        if ($model->load(Yii::$app->request->post())){
            $oldPassword = $model->Password;
            $model->Password = md5($model->Password);
            $model->Foto = UploadedFile::getInstance($model, 'Foto');

            if ($model->Foto) {                
                $imageName = 'upload/usuario/'.date('YmdHis').'_'.$model->Foto;
                $model->Foto->saveAs($imageName);
                $model->Foto = $imageName;
            }

            if($model->save()){
                return $this->redirect(['consultor/index']);
            }            
                    
        } 

        $model->Password = $oldPassword;
        if ($id!=''){
            $model->IdConsultor = $id;
            $model->IdRol='3';
        }
        $model->setUrlRetorno('consultor/index');
        return $this->render('create', ['model' => $model]);
    }

    /**
     * Updates an existing Usuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldFoto = $model->Foto;

        if ($model->load(Yii::$app->request->post())){       
            $model->Foto = UploadedFile::getInstance($model, 'Foto');

            if ($model->Foto) {             
                $imageName = 'upload/usuario/'.date('YmdHis').'_'.$model->Foto;
                $model->Foto->saveAs($imageName);
                $model->Foto = $imageName;
            }
            else{
                $model->Foto = $oldFoto;
            }

            if($model->save()) {
                return $this->redirect(['index']);
            }

        }

        return $this->render('update', ['model' => $model,]);
    }

    /**
     * Deletes an existing Usuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPassword($id)
    {
        $usuario = $this->findModel($id);
        $usuario->Password = md5('123456');
        $usuario->save();

        return $this->redirect(['index']);
    }
    
     public function actionChangepassword() {
     

       $usuario=Yii::$app->user->identity;
       $usuario->scenario = 'changepassword';
       $loadedPost=$usuario->load(Yii::$app->request->post());
       if($loadedPost && $usuario->validate()){
           $usuario->Password = md5($usuario->newPassword);
           $usuario->save(false);
           Yii::$app->session->setFlash('success','Se cambio la password');
           $this->redirect(array('site/logout'));
       }
       return $this->render("changepassword",
               ['usuario'=>$usuario]);
    }

    public function actionDeletefoto(){
        $id = $_POST['id'];
        $usuario = $this->findModel($id);
        $usuario->Foto = null;
        $usuario->save();
    }

    /**
     * Displays a single Empresa model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        $model->setUrlRetorno('consultor/index');
        return $this->render('view', [
            'model' => $model,
        ]);
    }
}
