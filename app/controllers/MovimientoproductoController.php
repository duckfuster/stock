<?php

namespace app\controllers;

use Yii;
use app\models\MovimientoProducto;
use app\models\Usuario;
use app\models\MovimientoProductoSearch;
use app\models\Existencia;
use app\models\Producto;
use app\models\Motivo;
use app\models\Remito;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\vendor\base\BaseController;
use app\models\ProductoSearch;
use app\models\Rol;

/**
 * MovimientoproductoController implements the CRUD actions for MovimientoProducto model.
 */
class MovimientoproductoController extends BaseController
{
   
    /**
     * Lists all MovimientoProducto models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MovimientoProductoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    /**
     * Lists all MovimientoProducto models.
     * @return mixed
     */
    public function actionIndex2($idrem=null)
    {
        
        $searchModel = new MovimientoProductoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$idrem);

        return $this->render('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'idrem'=>$idrem,
            
        ]);
    }

    /**
     * Displays a single MovimientoProducto model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model->cantidad=0;
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MovimientoProducto model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($idrem=null)
    {
        $remito=Remito::findOne($idrem);
        
        $model = new MovimientoProducto();
        
        // seteo remito
        $model->IdRemito=$idrem;

        // seteo fecha
        $model->Fecha = date("Y-m-d");  
        $request = Yii::$app->request;

        $idUser=Yii::$app->user->getId();
        /*$objUser = Usuario::findOne($idUser);        
        
        $model->IdUsuario=$idUser;
        
        if ($user->IdRol!=Rol::OPERADOR){
            $model->IdDestino = $user->IdDestino;
        }*/
        
        $mov = $request->get('mov'); 
        
        if($mov=="ent"){ // Entrada-> Deposito
            $model->TipoMovimiento = 0;            
            $model->IdDestino =1;
            $model->IdUsuario=$idUser;
        }
        if($mov=="sal"){
            $model->TipoMovimiento = 1; 
            $model->IdDestino = $remito->IdDestino;
            $model->IdUsuario=$remito->IdUsuario;
           
            
        }
        if($mov=="dev"){ // Devolucion -> Deposito
            $model->TipoMovimiento = 2;   
            $model->IdDestino =1;
            $model->IdUsuario=Rol::DEPOSITO;
        }

        if ($model->load(Yii::$app->request->post())) {
            // fechas
            $fecha =  date_create($model->Fecha);
            $model->Fecha = date_format($fecha,'Y-m-d');
            
            if($mov=="ent"||$mov=="sal"){
                $fecha =  date_create($model->Vencimiento);
                $model->Vencimiento = date_format($fecha,'Y-m-d');
            }else{
                $model->Vencimiento=null;
            }
            
           
            $existencia="";
             $count = Existencia::find()->where(['IdProducto' => $model->IdProducto])->count();
             if($count>0){ // update
                $existencia = Existencia::find()->where(['IdProducto' => $model->IdProducto])->one();
                //$existencia->IdDestino = $model->IdDestino;
                if($mov=="sal"){
                    if (($model->Cantidad)>($existencia->Cantidad)){
                        Yii::$app->session->setFlash('warning', 'La cantidad de salida es mayor al stock actual');
                        return $this->render('create',['model' => $model,'mov' => $request->get('mov'),'idrem'=>$model->IdRemito]);
                    }else{
                        $existencia->Cantidad = $existencia->Cantidad - $model->Cantidad;
                        //$existencia->save();
                    }
                    
                }else{ 
                    if($mov=="dev"){
                        $motivo = Motivo::find()->where(['IdMotivo' => $model->IdMotivo])->one();  
                        // si la dev es x cambio o ajuste, vuelvo a ingresar el producto al deposito 
                        if ($motivo->Motivo=="Cambio" || $motivo->Motivo=="Ajuste") {
                            $existencia->Cantidad = $existencia->Cantidad + $model->Cantidad;
                        }
                    }else if($mov=="ent"){
                        $existencia->Cantidad = $existencia->Cantidad + $model->Cantidad;
                    }                   
                }
             }else{ // new
                 if($mov=="sal"){
                     Yii::$app->session->setFlash('warning', 'Este producto no tiene stock en este momento');
                        return $this->render('create',['model' => $model,'mov' => $request->get('mov'),'idrem'=>$model->IdRemito]);
                 }
                $existencia = new Existencia();
                $existencia->IdProducto = $model->IdProducto;
                $existencia->Cantidad = $model->Cantidad;
               // $existencia->save();
             }
             
            if ($model->validate()) {
           
                 $model->save();
                 $existencia->save();
                 Yii::$app->session->setFlash('success', 'El registro ha sido guardado!');
                } else {
                    
                    $errors = $model->errors;
                    Yii::$app->session->setFlash('warning', 'Se genero un error: ');
                        return $this->render('create',['model' => $model,'mov' => $request->get('mov'),'idrem'=>$model->IdRemito]);
                }
    
                
             
             /*if ($model->save()&&$existencia->save()){
                 Yii::$app->session->setFlash('success', 'El registro ha sido guardado!');
             }*/
           /* Yii::$app->session->setFlash('success', 'El registro ha sido guardado!');
            $model->save();
            $existencia->save();*/


         if(isset($_POST['btn_mantener']))
            {   
                
                 return $this->redirect(['create','mov' => $request->get('mov'),'idrem'=>$model->IdRemito]);
                    
               
            }else {
                if ($mov=="sal"){
                    return $this->redirect(['index2','idrem'=>$model->IdRemito]);
                }else{
                    return $this->redirect(['/']);
                }
                
            }
            
        } else {
            return $this->render('create', [
                'model' => $model,
                'idrem'=>$idrem,
                'mov'=>$mov,
               
            ]);
        }
    }
    
    public function validarMov(){
        //create
        
        //update
        
        //delete
    }

    /**
     * Updates an existing MovimientoProducto model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // actualizar fecha en update
        
        
        $model = $this->findModel($id);
        $producto=Producto::findOne($model->IdProducto);
        
        $cantAnterior=$model->Cantidad;
        
      
        if ($model->load(Yii::$app->request->post())) {
            // fechas
            $fecha =  date_create($model->Fecha);
            $model->Fecha = date_format($fecha,'Y-m-d');
            
            if ($model->Cantidad!=$cantAnterior){ // se modifico la cant
                 $existencia = Existencia::find()->where(['IdProducto' => $model->IdProducto])->one();
                //$existencia->IdDestino = $model->IdDestino;
                if($model->TipoMovimiento==1){
                    if (($model->Cantidad)>($existencia->Cantidad)){
                        Yii::$app->session->setFlash('warning', 'La cantidad de salida es mayor al stock actual');
                        return $this->render('update',['id'=>$model->IdMovimiento,'model'=>$model]);
                    }else{
                        $existencia->Cantidad = $existencia->Cantidad - $model->Cantidad;
                        //$existencia->save();
                    }
                    
                }else{  
                   // $existencia->Cantidad = $existencia->Cantidad + $model->Cantidad;
                    if($mov=="dev"){
                        $motivo = Motivo::find()->where(['IdMotivo' => $model->IdMotivo])->one();  
                        // si la dev es x cambio o ajuste, vuelvo a ingresar el producto al deposito 
                        if ($motivo->Motivo=="Cambio" || $motivo->Motivo=="Ajuste") {
                            $existencia->Cantidad = $existencia->Cantidad + $model->Cantidad;
                        }
                    }                   
                    //$existencia->save();
                }
                
            }
            if ($model->validate()) {
           
                 $model->save();
                 $existencia->save();
                 Yii::$app->session->setFlash('success', 'El registro ha sido actualizado!');
                } else {
                    
                    $errors = $model->errors;
                    Yii::$app->session->setFlash('warning', 'Se genero un error: ');
                        return $this->render('create',['model' => $model,'mov' => $request->get('mov'),'idrem'=>$model->IdRemito]);
                }
                
                if ($mov=="sal"){
                    return $this->redirect(['index2','idrem'=>$model->IdRemito]);
                }else{
                    return $this->redirect(['/']);
                }
            
           
        } else {
            return $this->render('update', [
                'model' => $model,
                ''
            ]);
        }
    }

    /**
     * Deletes an existing MovimientoProducto model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MovimientoProducto model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MovimientoProducto the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MovimientoProducto::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
