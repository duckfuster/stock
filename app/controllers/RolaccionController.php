<?php

namespace app\controllers;

use Yii;
use app\models\Rolaccion;
use app\models\RolaccionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\vendor\base\BaseController;

/**
 * RolaccionController implements the CRUD actions for Rolaccion model.
 */
class RolaccionController extends BaseController
{
   

    /**
     * Lists all Rolaccion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RolaccionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rolaccion model.
     * @param string $IdRol
     * @param string $IdAccion
     * @return mixed
     */
    public function actionView($IdRol, $IdAccion)
    {
        return $this->render('view', [
            'model' => $this->findModel($IdRol, $IdAccion),
        ]);
    }

    /**
     * Creates a new Rolaccion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Rolaccion();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'IdRol' => $model->IdRol, 'IdAccion' => $model->IdAccion]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Rolaccion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $IdRol
     * @param string $IdAccion
     * @return mixed
     */
    public function actionUpdate($IdRol, $IdAccion)
    {
        $model = $this->findModel($IdRol, $IdAccion);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'IdRol' => $model->IdRol, 'IdAccion' => $model->IdAccion]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Rolaccion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $IdRol
     * @param string $IdAccion
     * @return mixed
     */
    public function actionDelete($IdRol, $IdAccion)
    {
        $this->findModel($IdRol, $IdAccion)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Rolaccion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $IdRol
     * @param string $IdAccion
     * @return Rolaccion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($IdRol, $IdAccion)
    {
        if (($model = Rolaccion::findOne(['IdRol' => $IdRol, 'IdAccion' => $IdAccion])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
