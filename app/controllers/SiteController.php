<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use app\models\LoginForm;
use app\models\Usuario;
use app\models\Usuariologin;
use app\vendor\base\BaseController;

class SiteController extends BaseController
{

    public function actionIndex()
    {        
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $idUsuario = Yii::$app->user->getId();                    
            $usuarioLogin = new Usuariologin();
            $usuarioLogin->IdUsuario = $idUsuario;
            $usuarioLogin->Ip = Yii::$app->request->getUserIp();
            $usuarioLogin->SessionId = Yii::$app->session->getId();
            $usuarioLogin->FechaHoraLogin = date('Y-m-d H:i:s');
            $usuarioLogin->FechaHoraLogout = null;
            $usuarioLogin->save();
            $usuario = Usuario::findOne($idUsuario); 
            $session = Yii::$app->session;
            $session->set('IdRol', $usuario->IdRol);
            $session->set('NombreApellido', $usuario->Nombre.', '.$usuario->Apellido);
            $this->InitializeMenu();
            $this->InitializeAcciones();
            return $this->goBack();
        } 
        else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        $login = Usuariologin::find()->where(['IdUsuario' => Yii::$app->user->getId()])->orderBy('IdUsuarioLogin DESC')->limit(1)->all();

        if(count($login)){
            $login[0]->FechaHoraLogout = date('Y-m-d H:i:s');
            $login[0]->save();
        }

        $session = Yii::$app->session;
        $session->remove('IdRol');
        $session->remove('NombreApellido');
        $session->remove('Menu');
        $session->remove('Acciones');
        Yii::$app->user->logout();        
        return $this->goHome();
    }

    public function actionError(){
        $exception = Yii::$app->errorHandler->exception;

        if ($exception !== null) {
            $this->LogError($exception);

            if (Yii::$app->request->isAjax){
                echo $exception->getMessage();
            }
            else{
                return $this->render('error', ['exception' => $exception, 'name' => $exception->getName(), 'message' => $exception->getMessage()]);
            }                
            
        }

    }
    
    public function actionAbout(){
        return $this->render('about');
    }

}
