<?php

namespace app\models;

use Yii;
use app\vendor\base\BaseActiveRecord;

/**
 * This is the model class for table "numeracion".
 *
 * @property integer $IdNumeracion
 * @property string $TipoDocumento
 * @property integer $Numeracion
 */
class Numeracion extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'numeracion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TipoDocumento', 'Numeracion'], 'required'],
            [['Numeracion'], 'integer'],
            [['TipoDocumento'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdNumeracion' => 'Id Numeracion',
            'TipoDocumento' => 'Tipo Documento',
            'Numeracion' => 'Numeracion',
        ];
    }
}
