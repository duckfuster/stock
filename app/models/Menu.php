<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property integer $IdMenu
 * @property string $Nombre
 * @property integer $IdAccion
 * @property integer $Activo
 * @property integer $IdMenuPadre
 * @property integer $Orden
 *
 * @property Accion $idAccion
 * @property Menu $idMenuPadre
 * @property Menu[] $menus
 */
class Menu extends \app\vendor\base\BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre', 'Activo', 'Orden'], 'required'],
            [['IdAccion', 'Activo', 'IdMenuPadre', 'Orden'], 'integer'],
            [['Nombre'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdMenu' => 'Id Menu',
            'Nombre' => 'Nombre',
            'IdAccion' => 'Accion',
            'Activo' => 'Activo',
            'IdMenuPadre' => 'Menu Padre',
            'Orden' => 'Orden',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAccion()
    {
        return $this->hasOne(Accion::className(), ['IdAccion' => 'IdAccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdMenuPadre()
    {
        return $this->hasOne(Menu::className(), ['IdMenu' => 'IdMenuPadre']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['IdMenuPadre' => 'IdMenu']);
    }
    
    public function getAccion(){
        if(isset($this->idAccion) || is_object($this->idAccion)){
            return $this->idAccion->Nombre;
        }else{
            return"";
        }
        
    }
    
    public function getMenuPadre(){
        if(isset($this->idMenuPadre) || is_object($this->idMenuPadre)){
            return $this->idMenuPadre->Nombre;
        }else{
            return "";
        }
        
    }
    
     public function getActivoDesc() {

        if ($this->Activo == 1) {
            return "Si";
        } else {
            return "No";
        }
    }
}
