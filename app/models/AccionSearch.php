<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Accion;

/**
 * AccionSearch represents the model behind the search form about `app\models\Accion`.
 */
class AccionSearch extends Accion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdAccion', 'Activa', 'ActivaDesc'], 'integer'],
            [['Nombre', 'Url'], 'safe'],
        ];
    }

    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['ActivaDesc']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Accion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['Nombre'=>SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->sort->attributes['ActivaDesc'] = [
            'asc' => ['Activa' => SORT_ASC],
            'desc' => ['Activa' => SORT_DESC],
        ];

        $query->andFilterWhere([
            'IdAccion' => $this->IdAccion,
            'Activa' => $this->ActivaDesc,
        ]);

        $query->andFilterWhere(['like', 'Nombre', $this->Nombre])
            ->andFilterWhere(['like', 'Url', $this->Url]);

        return $dataProvider;
    }
}
