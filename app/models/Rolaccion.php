<?php

namespace app\models;

use Yii;
use app\vendor\base\BaseActiveRecord;

/**
 * This is the model class for table "rolaccion".
 *
 * @property string $IdRol
 * @property string $IdAccion
 *
 * @property Accion $idAccion
 * @property Rol $idRol
 */
class Rolaccion extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rolaccion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdRol', 'IdAccion'], 'required'],
            [['IdRol', 'IdAccion'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdRol' => 'Id Rol',
            'IdAccion' => 'Id Accion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAccion()
    {
        return $this->hasOne(Accion::className(), ['IdAccion' => 'IdAccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRol()
    {
        return $this->hasOne(Rol::className(), ['IdRol' => 'IdRol']);
    }
}
