<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "auditoria".
 *
 * @property string $IdAuditoria
 * @property string $IdUsuario
 * @property string $FechaHora
 * @property string $Ip
 * @property string $SessionId
 * @property string $Tabla
 * @property string $PrimaryKey
 * @property string $TipoMovimiento
 * @property string $DatoAnterior
 * @property string $DatoNuevo
 *
 * @property Usuario $idUsuario
 */
class Auditoria extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auditoria';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdUsuario', 'FechaHora', 'Ip', 'SessionId', 'Tabla', 'PrimaryKey', 'TipoMovimiento'], 'required'],
            [['IdUsuario'], 'integer'],
            [['FechaHora'], 'safe'],
            [['DatoAnterior', 'DatoNuevo'], 'string'],
            [['Ip'], 'string', 'max' => 15],
            [['SessionId', 'Tabla', 'PrimaryKey'], 'string', 'max' => 50],
            [['TipoMovimiento'], 'string', 'max' => 1]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdAuditoria' => 'Id Auditoria',
            'IdUsuario' => 'Id Usuario',
            'FechaHora' => 'Fecha Hora',
            'Ip' => 'Ip',
            'SessionId' => 'Session ID',
            'Tabla' => 'Tabla',
            'PrimaryKey' => 'Primary Key',
            'TipoMovimiento' => 'Tipo Movimiento',
            'DatoAnterior' => 'Dato Anterior',
            'DatoNuevo' => 'Dato Nuevo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdUsuario()
    {
        return $this->hasOne(Usuario::className(), ['IdUsuario' => 'IdUsuario']);
    }
}
