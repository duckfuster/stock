<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Producto;

/**
 * ProductoSearch represents the model behind the search form about `app\models\Producto`.
 */
class ProductoSearch extends Producto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdProducto', 'IdProveedor','Proveedores', 'IdMarca', 'Marcas','IdTipoProducto','TipoProductos','IdTipoPresentacion'], 'integer'],
            [['CodBarra', 'Descripcion', ], 'safe'],
        ];
    }

   public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['Proveedores'],['Marcas'],['TipoProductos']);
    }
 
    
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$codBarra=null)
    {
        $query = Producto::find();
        //$query->innerJoinWith(['idProveedor' => function($query) { $query->from(['Proveedor' => 'proveedor']); }]);
      //  $query->innerJoinWith(['idMarca' => function($query) { $query->from(['marca']); }]);
     //   $query->innerJoinWith(['idTipoProducto' => function($query) { $query->from(['tipoproducto' ]); }]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([        
            'IdProveedor' => $this->Proveedores,
            'IdMarca' => $this->Marcas,
            'IdTipoProducto' => $this->TipoProductos,
            'Proveedor.IdProveedor' => $this->getAttribute('Proveedor')
        ]);
        
        if ($codBarra==null){
            $query->andFilterWhere(['like', 'CodBarra', $this->CodBarra])
                    ->andFilterWhere(['like', 'Descripcion', $this->Descripcion]);
        }else{
            $query->andFilterWhere(['like', 'CodBarra', $codBarra])
                    ->andFilterWhere(['like', 'Descripcion', $this->Descripcion]);
        }
        

        return $dataProvider;
    }
}
