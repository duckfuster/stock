<?php

namespace app\models;
use app\vendor\base\BaseActiveRecord;
use Yii;

/**
 * This is the model class for table "marca".
 *
 * @property integer $IdMarca
 * @property string $Marca
 */
class Marca extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'marca';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Marca'], 'required'],
            [['IdMarca'], 'integer'],
            [['Marca'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdMarca' => 'Id Marca',
            'Marca' => 'Marca',
        ];
    }
}
