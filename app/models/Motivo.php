<?php

namespace app\models;
use app\vendor\base\BaseActiveRecord;

use Yii;

/**
 * This is the model class for table "motivo".
 *
 * @property integer $IdMotivo
 * @property string $Motivo
 */
class Motivo extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'motivo';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Motivo'], 'required'],
            [['Motivo'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdMotivo' => 'Id Motivo',
            'Motivo' => 'Motivo',
        ];
    }
}
