<?php

namespace app\models;

use Yii;
use app\vendor\base\BaseActiveRecord;

/**
 * This is the model class for table "destino".
 *
 * @property integer $IdDestino
 * @property string $Destino
 */
class Destino extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'destino';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Destino'], 'required'],
            [['Destino'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdDestino' => 'Id Destino',
            'Destino' => 'Ruta',
        ];
    }
}
