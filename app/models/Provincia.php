<?php

namespace app\models;

use Yii;
use app\vendor\base\BaseActiveRecord;
/**
 * This is the model class for table "provincia".
 *
 * @property string $IdProvincia
 * @property string $Nombre
 *
 * @property Localidad[] $localidads
 */
class Provincia extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'provincia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required'],
            [['Nombre'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdProvincia' => 'Id Provincia',
            'Nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidads()
    {
        return $this->hasMany(Localidad::className(), ['IdProvincia' => 'IdProvincia']);
    }
}
