<?php

namespace app\models;
use app\vendor\base\BaseActiveRecord;

use Yii;

/**
 * This is the model class for table "tipopresentacion".
 *
 * @property integer $IdTipoPresentacion
 * @property integer $Descripcion
 */
class Tipopresentacion extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipopresentacion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Descripcion'], 'required'],
            [['Descripcion'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdTipoPresentacion' => 'Tipo Presentacion',
            'Descripcion' => 'Descripcion',
        ];
    }
}
