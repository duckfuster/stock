<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Motivo;

/**
 * MotivoSearch represents the model behind the search form about `app\models\Motivo`.
 */
class MotivoSearch extends Motivo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdMotivo'], 'integer'],
            [['Motivo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Motivo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'IdMotivo' => $this->IdMotivo,
        ]);

        $query->andFilterWhere(['like', 'Motivo', $this->Motivo]);

        return $dataProvider;
    }
}
