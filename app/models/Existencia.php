<?php

namespace app\models;

use app\vendor\base\BaseActiveRecord;
use app\models\Producto;
use app\models\Destino;


use Yii;

/**
 * This is the model class for table "existencia".
 *
 * @property integer $IdExistencia
 * @property integer $IdProducto
 * @property double $Precio
 * @property string $Vencimiento
 * @property integer $Cantidad
 * @property integer $TipoMovimiento
 */
class Existencia extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'existencia';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdProducto', 'Cantidad'], 'required'],
            [['IdProducto', 'Cantidad'], 'integer'],
            [['Precio',], 'double'] 
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdExistencia' => 'Id Existencia',
            'IdProducto' => 'Producto',
            'Precio' => 'Precio',
            'Vencimiento' => 'Vencimiento',
            'Cantidad' => 'Cantidad',            
        ];
    }

    public function getIdProducto()
    {
        return $this->hasOne(Producto::className(), ['IdProducto' => 'IdProducto']);
    }
    
    public function getProductos()
    {
        $return = "";
        $producto = Producto::findOne($this->IdProducto);
        return $producto->Descripcion;
        
        //return $this->IdProducto->Descripcion;
        
    }


    
}
