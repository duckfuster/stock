<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Existencia;

/**
 * ExistenciaSearch represents the model behind the search form about `app\models\Existencia`.
 */
class ExistenciaSearch extends Existencia
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdExistencia', 'IdProducto','Producto', 'TipoProducto','Cantidad', ], 'integer'],
            [['Producto'], 'safe'],
        ];
    }
     public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['Producto'],['TipoProducto']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }
  
    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Existencia::find();
        $query->innerJoinWith(['idProducto' => function($query) { $query->from(['Producto' => 'producto']); }]);
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['Descripcion'=>SORT_ASC]],
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

     /*    $dataProvider->sort->attributes['Producto'] = [
            'asc' => ['Producto.Descripcion' => SORT_ASC],
            'desc' => ['Producto.Descripcion' => SORT_DESC],
        ];*/

        $query->andFilterWhere([
           'Existencia.IdProducto' => $this->IdProducto,
            //'IdProducto' => $this->getAttribute('Producto'),
            'Cantidad'=> $this->Cantidad,
            'idTipoProducto' => $this->getAttribute('TipoProducto'),
        ]);

       // $query->andFilterWhere(['like', 'Producto.Descripcion', $this->getAttribute('Producto.Descripcion')]);

        return $dataProvider;
    }
}
