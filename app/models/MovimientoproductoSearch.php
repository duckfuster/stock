<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\MovimientoProducto;

/**
 * MovimientoProductoSearch represents the model behind the search form about `app\models\MovimientoProducto`.
 */
class MovimientoProductoSearch extends MovimientoProducto
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdMovimiento', 'Cantidad', 'IdProducto','Producto', 'TipoMovimiento','IdDestino','Destinos'], 'integer'],
            [['Producto','Fecha', 'Vencimiento'], 'safe'],

        ];
    }
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['Producto'],['Destinos']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$remito=null)
    {
        //print_r($params);
        $query = MovimientoProducto::find();
       // $query->innerJoinWith(['idProducto' => function($query) { $query->from(['Producto' => 'producto']); }]);

        
         $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['Fecha'=>SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        /*$dataProvider->sort->attributes['Producto'] = [
            'asc' => ['Producto.Descripcion' => SORT_ASC],
            'desc' => ['Producto.Descripcion' => SORT_DESC],
        ];*/

        $session = Yii::$app->session;
        $idRol = $session->get('IdRol', 0);

        if ($idRol==Rol::DEPOSITO){
            $query->andFilterWhere([
                'IdMovimiento' => $this->IdMovimiento,
                'IdDestino' => $this->Destinos,
                'Fecha' => $this->Fecha,
                'IdProducto'=>$this->IdProducto,
              //  'Producto.IdProducto' => $this->getAttribute('Producto'),
                'IdRemito'=>$remito,
            ]);
        }else{
            $idUser=Yii::$app->user->getId();
            
            $query->andFilterWhere([
                'IdMovimiento' => $this->IdMovimiento,
                'IdDestino' => $this->Destinos,
                'Fecha' => $this->Fecha,  
                'IdProducto'=>$this->IdProducto,
                'IdUsuario'=>$idUser,
               // 'Producto.IdProducto' => $this->getAttribute('Producto'),
                'IdRemito'=>$remito,
            ]);
        }

       // $query->andFilterWhere(['like', 'CodBarra', $this->CodBarra])
        //    ->andFilterWhere(['like', 'Destino', $this->Destino]);

        return $dataProvider;
    }
}
