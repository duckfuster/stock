<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Menu;

/**
 * MenuSearch represents the model behind the search form about `app\models\Menu`.
 */
class MenuSearch extends Menu
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdMenu', 'IdAccion', 'Activo', 'IdMenuPadre', 'Orden','Accion','MenuPadre','ActivoDesc'], 'integer'],
            [['Nombre'], 'safe'],
        ];
    }
    
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['Accion','MenuPadre','ActivoDesc']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Menu::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $dataProvider->sort->attributes['Accion'] = [
            'asc' => ['Accion.Nombre' => SORT_ASC],
            'desc' => ['Accion.Nombre' => SORT_DESC],
        ];
        
        $dataProvider->sort->attributes['MenuPadre'] = [
            'asc' => ['MenuPadre.Nombre' => SORT_ASC],
            'desc' => ['MenuPadre.Nombre' => SORT_DESC],
        ];
        
        $dataProvider->sort->attributes['ActivoDesc'] = [
            'asc' => ['Activo' => SORT_ASC],
            'desc' => ['Activo' => SORT_DESC],
        ];

        $query->andFilterWhere([
            'IdMenu' => $this->IdMenu,
            'IdAccion' => $this->IdAccion,
            'Activo' => $this->Activo,
            'IdMenuPadre' => $this->IdMenuPadre,
            'Menu.IdMenuPadre' => $this->getAttribute('MenuPadre'),
            'Menu.IdAccion' => $this->getAttribute('Accion'),
            'Orden' => $this->Orden,
        ]);

        $query->andFilterWhere(['like', 'Nombre', $this->Nombre]);

        return $dataProvider;
    }
}
