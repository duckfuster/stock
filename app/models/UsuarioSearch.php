<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Usuario;

/**
 * UsuarioSearch represents the model behind the search form about `app\models\Usuario`.
 */
class UsuarioSearch extends Usuario
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdUsuario', 'IdRol', 'Activo', 'Rol', 'ActivoDesc'], 'integer'],
            [['Apellido', 'Nombre', 'Foto', 'Username', 'Password'], 'safe'],
        ];
    }

    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['Rol', 'ActivoDesc']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Usuario::find();
        $query->innerJoinWith(['idRol' => function($query) { $query->from(['Rol' => 'rol']); }]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['Apellido'=>SORT_ASC, 'Nombre'=>SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->sort->attributes['Rol'] = [
            'asc' => ['Rol.Nombre' => SORT_ASC],
            'desc' => ['Rol.Nombre' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['ActivoDesc'] = [
            'asc' => ['Activo' => SORT_ASC],
            'desc' => ['Activo' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['LastConnection'] = [
            'asc' => ['(select max(FechaHoraLogin) from usuariologin where IdUsuario=usuario.IdUsuario)' => SORT_ASC],
            'desc' => ['(select max(FechaHoraLogin) from usuariologin where IdUsuario=usuario.IdUsuario)' => SORT_DESC],
        ];

        $query->andFilterWhere([
            'IdUsuario' => $this->IdUsuario,
            'IdRol' => $this->IdRol,
            'Activo' => $this->ActivoDesc,
            'Rol.IdRol' => $this->getAttribute('Rol'),
        ]);

        $query->andFilterWhere(['like', 'Apellido', $this->Apellido])
            ->andFilterWhere(['like', 'Nombre', $this->Nombre])
            ->andFilterWhere(['like', 'Foto', $this->Foto])
            ->andFilterWhere(['like', 'Username', $this->Username])
            ->andFilterWhere(['like', 'Password', $this->Password]);

        return $dataProvider;
    }
}
