<?php

namespace app\models;

use app\models\Marca;
use app\models\TipoProducto;
use app\models\Proveedor;
use app\models\TipoPresentacion;
use app\vendor\base\BaseActiveRecord;

use Yii;

/**
 * This is the model class for table "producto".
 *
 * @property integer $IdProducto
 * @property integer $IdProveedor
 * @property integer $IdMarca
 * @property integer $IdTipoProducto
 * @property integer $IdTipoPresentacion
 * @property string $CodBarra
 * @property string $Descripcion
 * @property string $CantPresentacion
 * @property TipoProducto $idTipoProducto

 */
 
class Producto extends BaseActiveRecord       
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'IdMarca','IdTipoProducto','IdTipoPresentacion', 'CodBarra', 'Descripcion', 'CantPresentacion'], 'required'],
            [['IdProducto', 'IdProveedor', 'IdMarca', 'IdTipoProducto','IdTipoPresentacion','CantPresentacion'], 'integer'],
            [['CodBarra'], 'string', 'max' => 50],
            [['Descripcion'], 'string', 'max' => 150],
            [['CodBarra'], 'unique', 'targetAttribute' => ['CodBarra'], 'message' => 'El Codigo ingresado ya existe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdProducto' => 'Producto',
            'IdProveedor' => 'Proveedor',
            'IdMarca' => 'Marca',
            'IdTipoProducto' => 'Tipo Producto',
            'IdTipoPresentacion' => 'Tipo Presentacion',
            'CodBarra' => 'Codigo Barra',
            'Descripcion' => 'Descripcion',
            'TipoProductos'=>'Tipo Producto',
            'Marcas'=>'Marcas',
            'Proveedores'=>'Proveedores',
            'CantPresentacion' => 'Cantidad Presentacion',           
        ];
    }

    public function getIdProveedor()
    {
        return $this->hasOne(Proveedor::className(), ['IdProveedor' => 'IdProveedor']);
    }
     public function getProveedores()
    {
        return $this->idProveedor->RazonSocial;
    }
    public function getIdMarca()
    {
        return $this->hasOne(Marca::className(), ['IdMarca' => 'IdMarca']);
    }

    public function getMarcas()
    {
        return $this->idMarca->Marca;
    }

    public function getIdTipoProducto()
    {
        return $this->hasOne(TipoProducto::className(), ['IdTipoProducto' => 'IdTipoProducto']);
    }
    
    public function getTipoProductos()
    {
       // return $this->idTipoProducto->TipoProducto;
        $return = "";
        $tipoProducto = Tipoproducto::findOne($this->IdTipoProducto);
        return $tipoProducto->TipoProducto;
    }

     public function getIdTipoPresentacion()
    {
        return $this->hasOne(TipoPresentacion::className(), ['IdTipoPresentacion' => 'IdTipoPresentacion']);
    }
    
    public function getTipoPresentacion()
    {
        return $this->idTipoPresentacion->Descripcion;
    }
}
