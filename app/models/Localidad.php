
<?php

namespace app\models;

use Yii;
use app\vendor\base\BaseActiveRecord;

/**
 * This is the model class for table "localidad".
 *
 * @property string $IdLocalidad
 * @property string $Nombre
 * @property string $IdProvincia
 * @property string $CodigoPostal
 *
 * @property Provincia $idProvincia
 */
class Localidad extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'localidad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre', 'IdProvincia'], 'required'],
            [['IdProvincia'], 'integer'],
            [['Nombre'], 'string', 'max' => 100],
            [['CodigoPostal'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdLocalidad' => 'Id Localidad',
            'Nombre' => 'Nombre',
            'IdProvincia' => 'Provincia',
            'CodigoPostal' => 'Código Postal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProvincia()
    {
        return $this->hasOne(Provincia::className(), ['IdProvincia' => 'IdProvincia']);
    }
    
    public function getProvincia(){
        return $this->idProvincia->Nombre;
    }
}
