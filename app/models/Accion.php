<?php

namespace app\models;

use Yii;
use app\vendor\base\BaseActiveRecord;

/**
 * This is the model class for table "accion".
 *
 * @property string $IdAccion
 * @property string $Nombre
 * @property string $Url
 * @property string $Activa
 *
 * @property Menu[] $menus
 * @property Rolaccion[] $rolaccions
 * @property Rol[] $idRols
 */
class Accion extends BaseActiveRecord
{
    public $Disponible;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'accion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre', 'Url', 'Activa'], 'required'],
            [['Activa'], 'integer'],
            [['Nombre', 'Url'], 'string', 'max' => 100],
            [['Disponible'],'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdAccion' => 'Id Accion',
            'Nombre' => 'Nombre',
            'Url' => 'Url',
            'Activa' => 'Activa',
            'ActivaDesc' => 'Activa',
            'Disponible' => 'Disponible',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenus()
    {
        return $this->hasMany(Menu::className(), ['IdAccion' => 'IdAccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRolaccions()
    {
        return $this->hasMany(Rolaccion::className(), ['IdAccion' => 'IdAccion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdRols()
    {
        return $this->hasMany(Rol::className(), ['IdRol' => 'IdRol'])->viaTable('rolaccion', ['IdAccion' => 'IdAccion']);
    }

    public function getActivaDesc(){

        if($this->Activa==1){
            return "Si";
        }
        else{
            return "No";
        }

    }
    public function getDisponible(){
        return $this->Disponible;
    }
}
