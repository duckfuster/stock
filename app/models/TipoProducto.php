<?php

namespace app\models;
use app\vendor\base\BaseActiveRecord;
use Yii;

/**
 * This is the model class for table "tipoproducto".
 *
 * @property integer $IdTipoProducto
 * @property string $TipoProducto
 */
class Tipoproducto extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tipoproducto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TipoProducto'], 'required'],
            [['IdTipoProducto'], 'integer'],
            [['TipoProducto'], 'string', 'max' => 50],
            [['IdTipoProducto'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdTipoProducto' => 'Id Tipo Producto',
            'TipoProducto' => 'Tipo Producto',
        ];
    }
}
