<?php

namespace app\models;

use Yii;
use app\vendor\base\BaseActiveRecord;
use yii\data\ActiveDataProvider;
use app\models\Accion;
/**
 * This is the model class for table "rol".
 *
 * @property string $IdRol
 * @property string $Nombre
 *
 * @property Rolaccion[] $rolaccions
 * @property Accion[] $idAccions
 * @property Usuario[] $usuarios
 */
class Rol extends BaseActiveRecord
{
    const ADMINISTRADOR=1;
    const DEPOSITO=2;
    const GERENTE=3;
    const OPERADOR=4;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Nombre'], 'required'],
            [['Nombre'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdRol' => 'Id Rol',
            'Nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRolaccions()
    {
        return $this->hasMany(Rolaccion::className(), ['IdRol' => 'IdRol']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdAccions()
    {
        return $this->hasMany(Accion::className(), ['IdAccion' => 'IdAccion'])->viaTable('rolaccion', ['IdRol' => 'IdRol']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios()
    {
        return $this->hasMany(Usuario::className(), ['IdRol' => 'IdRol']);
    }

    public function ObtenerAcciones(){                
        $sql=Accion::findBySql('SELECT A.IdAccion,
                                       A.Nombre,
                                       ifnull((SELECT TRUE
                                                 FROM rolaccion RA
                                                WHERE RA.IdAccion = A.IdAccion AND RA.IdRol = :idrol),
                                              FALSE)
                                          AS Disponible
                                  FROM accion A
                                 WHERE A.Activa=1
                              ORDER BY A.Nombre DESC')->addParams([':idrol'=> $this->IdRol] );        
   //echo "<pre>";print_r($sql->all());exit();
        $dataProvider = new ActiveDataProvider([
            'query' => $sql,    
            'pagination'=>false,
        ]);
        return $dataProvider;
    }
    
    public static function checkRol($rol){
        $session = Yii::$app->session;
        $idRol = $session->get('IdRol', 0);
        
         if ($idRol==$rol){
             return true;
         }else{
             return false;
         }
    }
}
