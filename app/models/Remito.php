<?php

namespace app\models;

use Yii;
use app\vendor\base\BaseActiveRecord;
use app\models\Usuario;

/**
 * This is the model class for table "remito".
 *
 * @property integer $IdRemito
 * @property string $Fecha
 * @property integer $IdDestino
 * @property integer $IdUsuario
 * @property string $Numero
 */
class Remito extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'remito';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Fecha', 'IdDestino', 'IdUsuario','Numero'], 'required'],
            [['IdRemito', 'IdDestino','IdUsuario'], 'integer'],
            [['Fecha'], 'safe'],
            [['Numero'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdRemito' => 'Id Remito',
            'Fecha' => 'Fecha',
            'IdDestino' => 'Ruta',
            'IdUsuario' => 'Usuario',
            'Numero' => 'N° Remito',
            'Usurios'=>'Usuario',
        ];
    }
    
     public function getDestinos()
    {
       $return = "";
        $destino = Destino::findOne($this->IdDestino);
        return $destino->Destino;
    }
    
     public function getUsuarios()
    {
       $return = "";
        $usuario = Usuario::findOne($this->IdUsuario);
        return $usuario->Apellido;
    }
    
    public static function tieneMovimientos($remito)
    {
        $count = Movimientoproducto::find()->where(['IdRemito' => $remito])->count();
        if($count!=0){
            return true;
        }else{
            return false;
        }
    }
    
}
