<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Localidad;

/**
 * LocalidadSearch represents the model behind the search form about `app\models\Localidad`.
 */
class LocalidadSearch extends Localidad
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdLocalidad', 'IdProvincia', 'Provincia'], 'integer'],
            [['Nombre', 'CodigoPostal'], 'safe'],
        ];
    }
 public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['Provincia']);
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Localidad::find();
        $query->innerJoinWith(['idProvincia' => function($query) { $query->from(['Provincia' => 'provincia']); }]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['Nombre'=>SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
  $dataProvider->sort->attributes['Provincia'] = [
            'asc' => ['Provincia.Nombre' => SORT_ASC],
            'desc' => ['Provincia.Nombre' => SORT_DESC],
        ];
        
        $query->andFilterWhere([
            'IdLocalidad' => $this->IdLocalidad,
            'IdProvincia' => $this->IdProvincia,
            'Provincia.IdProvincia' => $this->getAttribute('Provincia'),
        ]);

        $query->andFilterWhere(['like', 'localidad.Nombre', $this->Nombre])
            ->andFilterWhere(['like', 'localidad.CodigoPostal', $this->CodigoPostal]);

        return $dataProvider;
    }
}
