<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Remito;

/**
 * RemitoSearch represents the model behind the search form about `app\models\Remito`.
 */
class RemitoSearch extends Remito
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['IdRemito', 'IdDestino','Destinos','Usuarios'], 'integer'],
            [['Fecha', 'Numero'], 'safe'],
        ];
    }
    
     public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['Destinos'],['Usuarios']);
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Remito::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['Numero'=>SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $session = Yii::$app->session;
        $idRol = $session->get('IdRol', 0);

        if ($idRol==Rol::DEPOSITO){
            
            $query->andFilterWhere([
            'IdRemito' => $this->IdRemito,
            'Fecha' => $this->Fecha,
            'IdDestino' => $this->Destinos,
            'IdUsuario' => $this->Usuarios,
        ]);
            
        }else{
            $idUser=Yii::$app->user->getId();
            
            $query->andFilterWhere([
            'IdRemito' => $this->IdRemito,
            'Fecha' => $this->Fecha,
            'IdDestino' => $this->Destinos,
            'IdUsuario' => $idUser,
        ]);
        }

        $query->andFilterWhere(['like', 'Numero', $this->Numero]);

        return $dataProvider;
    }
}
