<?php

namespace app\models;
use app\vendor\base\BaseActiveRecord;

use Yii;

/**
 * This is the model class for table "proveedor".
 *
 * @property integer $IdProveedor
 * @property string $RazonSocial
 * @property string $Cuit
 * @property string $Telefono
 * @property string $Direccion
 * @property string $Email
 */
class Proveedor extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proveedor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['RazonSocial', 'Cuit', 'Telefono', 'Direccion', 'Email'], 'required'],
            [['IdProveedor'], 'integer'],
            [['RazonSocial', 'Cuit', 'Telefono', 'Direccion', 'Email'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdProveedor' => 'Id Proveedor',
            'RazonSocial' => 'Razon Social',
            'Cuit' => 'Cuit',
            'Telefono' => 'Telefono',
            'Direccion' => 'Direccion',
            'Email' => 'Email',
        ];
    }
}
