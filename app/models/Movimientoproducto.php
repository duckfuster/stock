<?php

namespace app\models;

use app\vendor\base\BaseActiveRecord;
use app\models\Producto;
use app\models\Destino;

use Yii;

/**
 * This is the model class for table "movimientoproducto".
 *
 * @property integer $IdMovimiento
 * @property string $Fecha
 * @property integer $Cantidad
 * @property decimal $Precio
 * @property integer $IdProducto
 * @property integer $TipoMovimiento
 * @property integer $IdDestino
 * @property integer $IdRemito
 * @property integer $IdUsuario
 * @property string $Vencimiento
 * @property Producto $idProducto
 */
class Movimientoproducto extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'movimientoproducto';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Fecha', 'Cantidad', 'IdProducto','IdUsuario','IdDestino'], 'required'],
            [['Fecha', 'Vencimiento','IdMotivo'], 'safe'],
           //[['Fecha', 'Vencimiento'], 'date'],
            [['Precio'], 'double', 'min' => 0],
            [['Precio'], 'default', 'value' => 0],
            [['Cantidad'], 'integer', 'min' => 1],
           // [['Cantidad'], 'validarCantidad'],
            
            [['Cantidad', 'IdProducto', 'TipoMovimiento','IdDestino','IdUsuario','IdMotivo'], 'integer']
           
        ];
    }
    
    public function validarCantidad($attribute, $params)
    {
        if (!in_array($this->$attribute, ['30', '60'])) {
            $this->addError($attribute, 'La cantidad es Incorrecta".');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'IdMovimiento' => 'Id Movimiento',
            'Fecha' => 'Fecha',
            'CodBarra' => 'Codigo Barra',
            'Cantidad' => 'Cantidad',
            'IdProducto' => 'Producto',
            'TipoMovimiento' => 'Tipo Movimiento',
            'IdDestino' => 'Destino',
            'IdUsuario' => 'Usuario',
            'IdRemito' => 'Remito',
            'IdMotivo' => 'Motivo',
            'Vencimiento' => 'Vencimiento',           
        ];
    }


    public function getIdProducto()
    {
        return $this->hasOne(Producto::className(), ['IdProducto' => 'IdProducto']);
    }
    
    public function getProducto()
    {
        $return = "";
        $producto = Producto::findOne($this->IdProducto);
        return $producto->Descripcion;
        
        //return $this->IdProducto->Descripcion;
        
    }

    public function getIdDestino()
    {
        return $this->hasOne(Destino::className(), ['IdDestino' => 'IdDestino']);
        
    }
    
    public function getDestinos()
    {
       $return = "";
        $destino = Destino::findOne($this->IdDestino);
        return $destino->Destino;
    }

    public function getTipoMovimientos()
    {
        return ($this->TipoMovimiento=="0" ? "Entrada":($this->TipoMovimiento=="1" ? 'Salida' : 'Devolución'));    
    }
    
    public function  getCodBarra(){
         $return = "";
        $producto = Producto::findOne($this->IdProducto);
        return $producto->CodBarra;
    }
            
}
