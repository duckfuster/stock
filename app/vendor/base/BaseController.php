<?php
namespace app\vendor\base;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use PDO;
use app\models\Usuario;
use app\models\Rol;

class BaseController extends Controller
{

    public function behaviors()
    {	
    	$route = isset(Yii::$app->request->getQueryParams()['r']) ? Yii::$app->request->getQueryParams()['r'] : 'site/index';

    	if(!strpos($route, '/')){
    		$route .= '/index';
    	}

	$accion = basename($route);
        $actionsDeny = $this->getAcciones();       
        $rules = [];        

        if(isset($actionsDeny[$route])){
            $rules[] = ['allow' => false, 'actions' => [$accion],'roles' => ['@'],];
        }

        $rules[] = ['allow' => true, 'roles' => ['@'],];
        $rules[] = ['allow' => true, 'actions' => ['login'], 'roles' => ['?'],];

        return [
        	'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function InitializeMenu(){
        $menuItems = [];

        if(!Yii::$app->user->isGuest){
            $session = Yii::$app->session;
            $idRol = $session->get('IdRol', 0);
            $idUsuario = Yii::$app->user->getId();           
            $sql = "select
                      m.*,
                      a.Url
                    from
                      menu m left join
                      accion a on m.IdAccion = a.IdAccion
                    where
                      m.Activo=1 and
                      (m.IdAccion is null or a.Activa=1) and
                      (m.IdAccion is null or exists(select * from rolaccion where IdAccion=m.IdAccion and IdRol=$idRol)) /*and
                      (m.IdAccion is not null or (select count(*) from menu inner join accion on menu.IdAccion = accion.IdAccion inner join rolaccion on accion.IdAccion = rolaccion.IdAccion where IdMenuPadre=m.IdMenu and Activo=1 and accion.Activa=1 and rolaccion.IdRol=$idRol)>0)*/
                    order by 
                      m.IdMenuPadre, 
                      m.Orden, 
                      m.Nombre";              
            $command =  Yii::$app->db->createCommand($sql);
            $data = $command->queryAll();

            foreach ($data as $d) {

                if($d['Nombre']=='Home'){
                    $usuario = Usuario::findOne($idUsuario);              
                    $foto = !is_null($usuario->Foto) ? $usuario->Foto : "css/images/user.png";
                    $rol = Rol::findOne($idRol);
                    
                    $d['Nombre'] = ($usuario->Nombre).'('.($rol->Nombre).')';
                    $d['Options'] = ['style' => 'background-image: url("'.$foto.'"); background-size: 32px 32px; background-repeat: no-repeat; background-position: left center; padding-left: 28px;'];
                }

                $item = array('idmenu' => $d['IdMenu'], 'idmenupadre' => $d['IdMenuPadre'], 'label' => $d['Nombre'], 'url' => !is_null($d['Url']) ? [$d['Url']] : '#', 'options' => isset($d['Options']) ? $d['Options'] : []);

                if(is_null($d['IdMenuPadre'])){
                    $menuItems[] = $item;
                }
                else{
                    $this->setMenuChild($item, $menuItems);
                }

            }

        }

        $session = Yii::$app->session;
        $session->set('Menu', $menuItems);
    }

    public function setMenuChild($item, &$menuItems){
        $flag = false;
        $i = 0;

        while(!$flag and $i<count($menuItems)){

            if($menuItems[$i]['idmenu']==$item['idmenupadre']){
                $flag = true;
                
                if(!isset($menuItems[$i]['items'])){
                    $menuItems[$i]['items'] = [];
                }

                $menuItems[$i]['items'][] = $item;
            }
            else{

                if(isset($menuItems[$i]['items'])){

                    if(count($menuItems[$i]['items'])){
                        $flag = $this->setMenuChild($item, $menuItems[$i]['items']);
                    }    

                }

            }

            $i++;
        }

        return $flag;
    }

    public function InitializeAcciones(){
        $actionsDeny = [];

        if(!Yii::$app->user->isGuest){
            $idUsuario = Yii::$app->user->getId();
            $sql = "select * from accion where Activa=1 and not exists(select * from usuario u inner join rol r on u.IdRol = r.IdRol inner join rolaccion ra on r.IdRol = ra.IdRol where ra.IdAccion=accion.IdAccion and u.IdUsuario=:IdUsuario)";
            $command =  Yii::$app->db->createCommand($sql);
            $command->bindParam(':IdUsuario', $idUsuario);
        }
        else{
            $sql = "select * from accion where Activa=1";
            $command =  Yii::$app->db->createCommand($sql);
        }        
        
        $data = $command->queryAll();

        foreach ($data as $d) {
            $actionsDeny[$d['Url']] = 1;
        }

        $session = Yii::$app->session;
        $session->set('Acciones', $actionsDeny);
    }

    public function getMenu(){
        $menu = Yii::$app->session->get('Menu', false);
        return $menu ? $menu : [];
    }

    public function getAcciones(){
        $acciones = Yii::$app->session->get('Acciones', false);
        return $acciones ? $acciones : [];
    }

    public function LogError($exception){
        $username = 'Guest';

        if(!Yii::$app->user->isGuest){
            $idUsuario = Yii::$app->user->getId();
            $usuario = Usuario::findOne($idUsuario);
            $username = $usuario->username;
        }

        $fileName = "log/error.log";
        $content = date('YmdHis ') . "\t" . $username . "\t" . serialize(['code' => $exception->getCode(), 'name' => $exception->getName(), 'file' => $exception->getFile(), 'line' => $exception->getLine(), 'message' => $exception->getMessage()]) . "\n";
        $this->Log($fileName, $content, 1024 * 1024);
    }

    public function Log($fileName, $content, $maxSize) {

        try{
            
            if (file_exists($fileName)) {

                if (filesize($fileName) > $maxSize) {
                    rename($fileName, $fileName . "_" . date('YmdHis'));
                }
            }

            file_put_contents($fileName, $content, FILE_APPEND | LOCK_EX);
        }
        catch(exception $e){

        }

    }

}

?>