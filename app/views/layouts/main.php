<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\widgets\Alert;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Divanni - <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
    <script>
    window.setTimeout(function() {
    $(".alert").fadeTo(300, 0).slideUp(300, function(){
        $(this).remove(); 
    });
}, 5000);

    </script>
  <?php  $js = <<< 'SCRIPT'
$(function () { 
    $("a").tooltip(); 
});;
$(function () { 
    $("[data-toggle='popover']").popover(); 
});
$(function() {
    function reposition() {
        var modal = $(this),
            dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        
        // Dividing by two centers the modal exactly, but dividing by three 
        // or four works better for larger screens.
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    }
    // Reposition when a modal is shown
    $('.modal').on('show.bs.modal', reposition);
    // Reposition when the window is resized
    $(window).on('resize', function() {
        $('.modal:visible').each(reposition);
    });
});
SCRIPT;
$this->registerJs($js);

?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="row-fluid">
            <div class="col-lg-2">
                <?= Html::img('@web/images/') ?>
                <div style="padding-top: 150px">
  
                </div>
                <!--<img src="">-->
            </div>
            <div class="col-lg-10" style="    border-left: 1px solid #e7e7e7;background: #fff">
                <div class="wrap">
                    <?php
                    NavBar::begin([
//                       'brandLabel' => 'Cpyme Adeneu - Mejora Continua',
                    'brandUrl' => Yii::$app->homeUrl,
                        'options' => [
//                            'class' => 'navbar navbar ',
                        //  'class' => 'navbar navbar-default navbar-fixed-top',
                        ],
                    ]);

                    echo Nav::widget([
                        'options' => ['class' => 'navbar-nav navbar-right'],
                        'items' => Yii::$app->controller->getMenu(),
                    ]);

                    NavBar::end();
                    ?>

        
        <div id="page-wrapper">
              <?= Alert::widget() ?>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
          
            <?= $content ?>
        </div>
    </div>

              
            </div>
            
        </div>   

    <footer class="footer">
        <div class="container">
            <p class="pull-right">&copy; Divanni <?= date('Y') ?></p>
            <!--p class="pull-right">Desarrollado por <a href="http://www.solucionesitweb.com" target="_blank">Soluciones ITweb</a></p-->
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>