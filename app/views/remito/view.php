<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Remito */

//$this->title = $model->IdRemito;
$this->params['breadcrumbs'][] = ['label' => 'Remitos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remito-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
       
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'Fecha',
            'IdDestino',
            'Numero',
        ],
    ]) ?>

</div>
