<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use app\models\Usuario;
use app\models\Destino;

/* @var $this yii\web\View */
/* @var $model app\models\Remito */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="remito-form">

    <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-5'>{input}</div></div>"]]); ?>


    <?=$form->field($model, 'Fecha', ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-2'>{input}</div></div>"])
            ->widget(DatePicker::classname(), [
        'language' => 'es',
        'dateFormat' => 'yyyy-MM-dd',
        'options' => ['class' => 'form-control'],
    ])
    ?>

    <?= $form->field($model, 'IdDestino')->dropDownList(ArrayHelper::map(Destino::find()->all(),'IdDestino','Destino'),['prompt'=>'Seleccione']) ?>
    
     <?= $form->field($model, 'IdUsuario')->dropDownList(ArrayHelper::map(Usuario::find()->all(),'IdUsuario','Apellido'),['prompt'=>'Seleccione']) ?>

    <?= $form->field($model, 'Numero')->textInput(['maxlength' => true,'disabled' => 'disabled']) ?>

    <div class="form-group">
        <div class="col-xs-1" ><?= Html::a('Cancelar', Url::to( ['remito/index']), ['class' => 'btn btn-danger', 'data-confirm' => '¿Desea cancelar los cambios?']) ?></div>
        <div class="col-xs-1" ><?= Html::submitButton($model->isNewRecord ? 'Guardar y Salir' : 'Guardar',['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?></div>
      
    </div>

    <?php ActiveForm::end(); ?>

</div>
