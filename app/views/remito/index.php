<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Destino;
use app\models\Usuario;
use app\models\Remito;
use app\models\Rol;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RemitoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Remitos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remito-index">

    <h2><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Nuevo Remito', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            
            'Numero',

            [
            'attribute' => 'Fecha',
           'format'=>['Date','dd-MM-yyyy']
            ],
            [
                'attribute' => 'Destinos',
                'filter' => ArrayHelper::map(Destino::find()->orderBy(['Destino'=>SORT_ASC])->all(), 'IdDestino', 'Destino'),
            ],
            [
                'attribute' => 'Usuarios',
                'filter' => ArrayHelper::map(Usuario::find()->orderBy(['Apellido'=>SORT_ASC])->all(), 'IdUsuario', 'Apellido'),
            ],
            

           ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                'productos' => function ($url, $model, $key) {
                    //$url='movimientoproducto%2Fcreate&mov=sal';
                    $url = Url::to(['movimientoproducto/index2', 'idrem' => $key]);
                    return Html::a('<i class="fa fa-list-ol"></i>', $url, ['title' => 'Cargar Productos', 'data-toggle' => 'tooltip']);
                },
                        
                'update' => function ($url, $model, $key) {
                    //$url='movimientoproducto%2Fcreate&mov=sal';
                    $url = Url::to(['remito/update', 'id' => $key]);
                    return ((!Remito::tieneMovimientos($key)&&Rol::checkRol(Rol::DEPOSITO))? Html::a('<i class="fa fa-pencil"></i>', $url, ['title' => 'Editar Remito', 'data-toggle' => 'tooltip']):"");
                   // (($model->findPlanesAsociados($key)) ? Html::a('<i class="fa fa-calendar"></i>', $create, ['title' => 'Planes Empresa', 'data-toggle' => 'tooltip']) : "");
                },        
                        
                        
              ],
                'template' => ' {update} {productos}' ,]          
               
        ],
    ]); ?>

</div>
