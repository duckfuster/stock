<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Rolaccion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rolaccion-form">

    <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-10'>{input}</div></div>"]]); ?>

    <?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'IdRol')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'IdAccion')->textInput(['maxlength' => 10]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
