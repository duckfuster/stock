<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RolaccionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rolaccions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rolaccion-index">

    <h2><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Rolaccion', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IdRol',
            'IdAccion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
