<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rolaccion */

$this->title = $model->IdRol;
$this->params['breadcrumbs'][] = ['label' => 'Rolaccions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rolaccion-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Update', ['update', 'IdRol' => $model->IdRol, 'IdAccion' => $model->IdAccion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'IdRol' => $model->IdRol, 'IdAccion' => $model->IdAccion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IdRol',
            'IdAccion',
        ],
    ]) ?>

</div>
