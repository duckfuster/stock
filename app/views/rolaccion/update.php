<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rolaccion */

$this->title = 'Update Rolaccion: ' . ' ' . $model->IdRol;
$this->params['breadcrumbs'][] = ['label' => 'Rolaccions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IdRol, 'url' => ['view', 'IdRol' => $model->IdRol, 'IdAccion' => $model->IdAccion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rolaccion-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
