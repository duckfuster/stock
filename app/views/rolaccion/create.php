<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rolaccion */

$this->title = 'Create Rolaccion';
$this->params['breadcrumbs'][] = ['label' => 'Rolaccions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rolaccion-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
