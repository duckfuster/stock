<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Proveedor;
use app\models\Marca;
use app\models\TipoProducto;
use app\models\TipoPresentacion;
/* @var $this yii\web\View */
/* @var $model app\models\Producto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="producto-form">


    <?php $form = ActiveForm::begin(['class'=>'form-inline form-group-xs'],['fieldConfig' => ['validateOnSubmit'=>false]]); ?>

    <?= $form->errorSummary($model) ?>
    

<div class="form-group form-group-xs">
    <label class="col-xs-3 control-label" for="CodBarra">Codigo Barra</label>
    <div class="col-xs-2">  
            <?= $form->field($model, 'CodBarra')->textInput(['maxlength' => true],['class' => 'form-control'])->label(false) ?>
    </div>       
</div>
<div class="row"></div>

<div class="form-group form-group-xs">
    <label class="col-xs-3 control-label" for="Descripcion">Descripcion</label>
    <div class="col-xs-4">  
            <?= $form->field($model, 'Descripcion')->textInput(['maxlength' => true],['class' => 'form-control'])->label(false) ?>
    </div>       
</div>
<div class="row"></div>
    
<div class="form-group form-group-xs">
    <label class="col-xs-3 control-label" for="IdProveedor">Proveedor</label>
    <div class="col-xs-3">  
            <?= $form->field($model, 'IdProveedor')->dropDownList(ArrayHelper::map(Proveedor::find()->all(),'IdProveedor','RazonSocial'),['class' => 'form-control'])->label(false) ?>
    </div>
    <span class="input-group-btn">
         <?= Html::a('+', Url::to( ['proveedor/create','env' => 'producto']), ['class' => 'btn btn-success']) ?>
    </span>
</div>
<div class="row"></div>

<div class="form-group form-group-xs">
    <label class="col-xs-3 control-label" for="IdMarca">Marca</label>
    <div class="col-xs-3">
           <?= $form->field($model, 'IdMarca')->dropDownList(ArrayHelper::map(Marca::find()->all(),'IdMarca','Marca'),['class' => 'form-control'])->label(false) ?>
    </div>
    <span class="input-group-btn">
            <?= Html::a('+', Url::to( ['marca/create','env' => 'producto']), ['class' => 'btn btn-success']) ?>
    </span>
</div>
<div class="row"></div>

<div class="form-group form-group-xs">
    <label class="col-xs-3 control-label" for="IdTipoProducto">Tipo Producto</label>
    <div class="col-xs-3">  
            <?= $form->field($model, 'IdTipoProducto')->dropDownList(ArrayHelper::map(TipoProducto::find()->all(),'IdTipoProducto','TipoProducto'),['class' => 'form-control'])->label(false) ?>
    </div>
    <span class="input-group-btn">
        <?= Html::a('+', Url::to( ['tipo-producto/create','env' => 'producto']), ['class' => 'btn btn-success']) ?>
    </span>
</div>
<div class="row"></div>

<div class="form-group form-group-xs">
    <label class="col-sm-3 control-label" for="IdTipoPresentacion">Tipo Presentaci&oacuten</label>
    <div class="col-sm-3">
           <?= $form->field($model, 'IdTipoPresentacion')->dropDownList(ArrayHelper::map(TipoPresentacion::find()->all(),'IdTipoPresentacion','Descripcion'),['class' => 'form-control'])->label(false) ?>
    </div>      
</div>
<div class="row"></div>

 <div class="form-group form-group-xs">
    <label class="col-xs-3 control-label" for="CantPresentacion">Cant. Presentaci&oacuten</label>
    <div class="col-xs-2">
       <?= $form->field($model, 'CantPresentacion')->textInput(['maxlength' => true],['class' => 'form-control'])->label(false)?>
    </div>
 </div>
<div class="row"></div>

<div class="form-group form-group-sm">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancelar', Url::to( ['producto/index']), ['class' => 'btn btn-danger', 'data-confirm' => '¿Desea cancelar los cambios?']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
