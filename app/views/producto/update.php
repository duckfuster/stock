<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Producto */

$this->title = 'Editar Producto: ' . ' ' . $model->Descripcion;
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IdProducto, 'url' => ['view', 'id' => $model->IdProducto]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="producto-update">

     <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?= Html::encode($this->title) ?></h3>
                </div>
                <!-- /.col-lg-12 -->
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
