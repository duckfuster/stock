<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Producto */

$this->title = 'Agregar Producto';
$this->params['breadcrumbs'][] = ['label' => 'Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-create">
    <div class="row">
        <div class="col-lg-4">
            
        </div>
    </div>
	<div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?= Html::encode($this->title) ?></h3>
                    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
                </div>
                <!-- /.col-lg-12 -->
    </div>

    

</div>
