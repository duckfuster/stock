<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Proveedor;
use app\models\Marca;
use app\models\TipoProducto;
use app\models\TipoPresentacion;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-index">
 <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?= Html::encode($this->title) ?></h3>
                </div>
                <!-- /.col-lg-12 -->
    </div>
    <p>
        <?= Html::a('Agregar Producto', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?php
    $gridColumns = [
             'CodBarra',
             'Descripcion',
             [
                'attribute' => 'Proveedores',
                 'filter' => ArrayHelper::map(Proveedor::find()->orderBy(['RazonSocial'=>SORT_ASC])->all(), 'IdProveedor', 'RazonSocial'),                
             ],             
             [
                'attribute' => 'Marcas',
                'filter' => ArrayHelper::map(Marca::find()->orderBy(['Marca'=>SORT_ASC])->all(), 'IdMarca', 'Marca'),
             ],
             [
                'attribute' => 'TipoProductos',
                'filter' => ArrayHelper::map(TipoProducto::find()->orderBy(['TipoProducto'=>SORT_ASC])->all(), 'IdTipoProducto', 'TipoProducto'),
             ],
            // 'descripcion',
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}',]
        ];
    
    echo GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsive' => true,
                'hover' => true,
                'autoXlFormat' => true,
                'panel' => [
                    'type' => GridView::TYPE_DEFAULT,
                    'footer' => false
                ],
                'toolbar' => [
                    '{export}', '{toggleData}'
                ],
                'columns' => $gridColumns,
                'exportConfig' => [
                    GridView::EXCEL => [
                        'filename' => 'productos_' . date('YmdHis'),
                        'options' => ['title' => 'Microsoft Excel 95+'],
                        'mime' => 'application/vnd.ms-excel',
                    ],
                    GridView::PDF => [
                        'filename' => 'productos_' . date('YmdHis'),
                        'mime' => 'application/pdf',
                        'config' => [
                            'format' => 'A4-P',
                            'destination' => 'I',
                            'mode' => 'utf-8',
                            'methods' => [
                                'SetHTMLHeader' => '<div style="text-aling: left; font-weight: bold;display:inline-block">' . Html::encode($this->title) . '</div>',
                                'SetFooter' => ['{PAGENO}'],
                            ],
                            'cssInline' => '.progress-bar,.progress-bar-warning{background-color:#fff !important;color:#333}'
                        ],
                        'showPageSummary' => FALSE,
                        'showCaption' => FALSE,
                    ]
                ],
                'export' => [
                    'fontAwesome' => false,
                    'showConfirmAlert' => false,
                    'label' => 'Exportar',
                    'target' => GridView::TARGET_BLANK,
                ],
            ]);
            
            
            
        ?>

    

</div>
