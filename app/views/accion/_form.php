<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Accion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accion-form">

	<?php $form = ActiveForm::begin(['fieldConfig' => ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-10'>{input}</div></div>"]]); ?>

    <?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'Url')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'Activa')->checkBox() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancelar', Url::toRoute('accion/index'), ['class' => 'btn btn-danger', 'data-confirm' => '¿Desea cancelar los cambios?']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
