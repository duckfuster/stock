<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Accion */

$this->title = $model->IdAccion;
$this->params['breadcrumbs'][] = ['label' => 'Accions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="accion-view">

    <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?= Html::encode($this->title) ?></h3>
                </div>
                <!-- /.col-lg-12 -->
    </div>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->IdAccion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->IdAccion], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IdAccion',
            'Nombre',
            'Url:url',
            'Activa',
        ],
    ]) ?>

</div>
