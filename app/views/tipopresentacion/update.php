<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tipopresentacion */

$this->title = 'Update Tipopresentacion: ' . ' ' . $model->IdTipoPresentacion;
$this->params['breadcrumbs'][] = ['label' => 'Tipopresentacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IdTipoPresentacion, 'url' => ['view', 'id' => $model->IdTipoPresentacion]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tipopresentacion-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
