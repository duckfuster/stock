<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Tipopresentacion */

$this->title = 'Create Tipopresentacion';
$this->params['breadcrumbs'][] = ['label' => 'Tipopresentacions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tipopresentacion-create">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
