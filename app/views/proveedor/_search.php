<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProveedorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="proveedor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'IdProveedor') ?>

    <?= $form->field($model, 'RazonSocial') ?>

    <?= $form->field($model, 'Cuit') ?>

    <?= $form->field($model, 'Telefono') ?>

    <?= $form->field($model, 'Direccion') ?>

    <?php // echo $form->field($model, 'Email') ?>

      <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Limpiar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
