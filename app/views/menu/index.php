<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Accion;
use app\models\Menu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Agregar Menu', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'Nombre',
           
           [
                'attribute' => 'Accion',
                'filter' => ArrayHelper::map(Accion::find()->orderBy(['Nombre'=>SORT_ASC])->all(), 'IdAccion', 'Nombre'),
            ],
            [
                'attribute' => 'ActivoDesc',
                'filter' => ['No', 'Si'],
            ],
           [
                'attribute' => 'MenuPadre',
                'filter' => ArrayHelper::map(Menu::find()->orderBy(['Nombre'=>SORT_ASC])->all(), 'IdMenu', 'Nombre'),
            ],
            'Orden',

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',],
        ],
    ]); ?>

</div>
