<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Accion;
use app\models\Menu;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-form">

     <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-4'>{input}</div></div>"],'validateOnSubmit'=>false]); ?>


    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => true]) ?>
    
    
     <?= $form->field($model, 'IdAccion')->dropDownList(ArrayHelper::map(Accion::find()->all(),'IdAccion','Nombre')) ?>

    
    <?= $form->field($model, 'Activo')->checkbox() ?>

    <?php
    
           $miArray=ArrayHelper::map(Menu::find()->all(),'IdMenu','Nombre');
           $arr1 = array('0' => NULL) + $miArray;
           echo ($form->field($model, 'IdMenuPadre')->dropDownList($arr1));
    
    ?>

    <?= $form->field($model, 'Orden')->textInput() ?>

    <div class="form-group">
         <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancelar', Url::to( ['menu/index']), ['class' => 'btn btn-danger', 'data-confirm' => '¿Desea cancelar los cambios?']) ?>
    
    </div>

    <?php ActiveForm::end(); ?>

</div>
