<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Destino */

$this->title = 'Agregar Ruta';
$this->params['breadcrumbs'][] = ['label' => 'Ruta', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="destino-create">

   	<div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><?= Html::encode($this->title) ?></h3>
        </div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
