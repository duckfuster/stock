<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Destino */

$this->title = 'Editar Ruta: ' . ' ' . $model->Destino;
$this->params['breadcrumbs'][] = ['label' => 'Ruta', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->IdDestino, 'url' => ['view', 'id' => $model->IdDestino]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="destino-update">
 	<div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><?= Html::encode($this->title) ?></h3>
        </div>
    </div>
    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
