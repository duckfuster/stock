<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Destino */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="destino-form">

 
    <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-4'>{input}</div></div>"],'validateOnSubmit'=>false]); ?>

    <?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'Destino')->textInput(['maxlength' => true]) ?>

   
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancelar', Url::to( ['destino/index']), ['class' => 'btn btn-danger', 'data-confirm' => '¿Desea cancelar los cambios?']) ?>
    </div>

    <?php ActiveForm::end(); ?>


</div>
