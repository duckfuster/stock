<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DestinoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ruta';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="destino-index">

    <h2><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Agregar Ruta', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'Destino',
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}',]
        ],
    ]); ?>

</div>
