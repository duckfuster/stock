<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\models\Rol;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = "Usuario: ".$model->Username;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-view">

    <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?= Html::encode($this->title) ?></h3>
                </div>
                <!-- /.col-lg-12 -->
    </div>

    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'IdUsuario',
            'Apellido',
            'Nombre',
            'Foto',
            'Username',
            //'Password',
            [
                'attribute' => 'Rol',
                'filter' => ArrayHelper::map(Rol::find()->orderBy(['Nombre'=>SORT_ASC])->all(), 'IdRol', 'Nombre'),
            ],
            [
                'attribute' => 'ActivoDesc',
                'filter' => ['No', 'Si'],
            ],
            'LastConnection:datetime'
        ],
    ]) ?>

    <p>
        <?= Html::a('Volver', [$model->getUrlRetorno()], ['class' => 'btn btn-primary']) ?>
    </p>
    
</div>
