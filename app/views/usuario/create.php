<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = 'Agregar Usuario';
?>
<div class="usuario-create">

     <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?= Html::encode($this->title) ?></h3>
                </div>
                <!-- /.col-lg-12 -->
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
