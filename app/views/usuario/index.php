<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Rol;
use app\models\Destino;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
?>
<div class="usuario-index">

    <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?= Html::encode($this->title) ?></h3>
                </div>
                <!-- /.col-lg-12 -->
    </div>

    <p>
        <?= Html::a('Agregar Usuario', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'format' => 'raw',
                'header' => 'Foto',
                'value' => function($data) { 
                                return Html::img(!is_null($data->Foto) ? $data->Foto : 'css/images/user.png', ['style' => 'width: 64px; height: 64px;']);
                        },
            ],
            'Apellido',
            'Nombre',
            'Username',
            [
                'attribute' => 'Rol',
                'filter' => ArrayHelper::map(Rol::find()->orderBy(['Nombre'=>SORT_ASC])->all(), 'IdRol', 'Nombre'),
            ],
            [
                'attribute' => 'Ruta',
                'filter' => ArrayHelper::map(Destino::find()->orderBy(['Destino'=>SORT_ASC])->all(), 'IdDestino', 'Destino'),
            ],
            [
                'attribute' => 'ActivoDesc',
                'filter' => ['No', 'Si'],
            ],
            [
                'class' => 'yii\grid\ActionColumn', 
                'template' => '{password} {update} {delete}',
                'buttons' => [
                    'password' => function ($url, $model) {
                                    return Html::a('<span class="glyphicon glyphicon-lock"></span>', $url, [
                                        'title' => 'Reiniciar contraseña',
                                        'onclick' => 'return confirm(\'¿Desea reiniciar la contraseña del usuario? La nueva contraseña será: 123456\');'
                                    ]);
                                }
                ]
            ],
            'LastConnection:datetime'
        ],
    ]); ?>

</div>
