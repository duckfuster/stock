<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Rol;
use app\models\Destino;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="usuario-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'fieldConfig' => ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-10'>{input}</div></div>"]]); ?>

    <?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'Username')->textInput(['maxlength' => 20]) ?>

    <?php
        if($model->isNewRecord){
            echo $form->field($model, 'Password')->passwordInput(['maxlength' => 50]);
        }
    ?>

    <?= $form->field($model, 'Apellido')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'Foto')->fileInput() ?>

    <?php
        if(!is_null($model->Foto)){
    ?>
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-xs-2"></div>
                <div class="col-xs-10">
                    <?= Html::img($model->Foto, ['id' => 'imgFoto', 'class' => 'img img-responsive']) ?>
                </div>
            </div>
            <div class="row" style="margin-bottom: 10px;">
                <div class="col-xs-2"></div>
                <div class="col-xs-10">
                    <?= Html::a('Eliminar foto', '#', ['id' => 'btnEliminar', 'onclick' => 
                        "if(confirm('¿Desea eliminar la foto? Esta acción no se puede deshacer')){
                            $.ajax({
                                type: 'POST',
                                url: '".Url::toRoute('usuario/deletefoto')."',
                                data: {id: ".$model->IdUsuario."},
                                success: function(response) {
                                    $('#imgFoto').hide();
                                    $('#btnEliminar').hide();
                                }
                            });
                        }"
                    ]) ?>
                </div>
            </div>
    <?php            
        }
    ?>

    <?= $form->field($model, 'IdRol')->dropDownList(ArrayHelper::map(Rol::find()->orderBy(['Nombre'=>SORT_ASC])->all(), 'IdRol', 'Nombre'), ['prompt' => 'Seleccione']) ?>

    <?= $form->field($model, 'IdDestino')->dropDownList(ArrayHelper::map(Destino::find()->orderBy(['Destino'=>SORT_ASC])->all(), 'IdDestino', 'Destino'), ['prompt' => 'Seleccione']) ?>

    <?= $form->field($model, 'Activo')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancelar', Url::toRoute('usuario/index'), ['class' => 'btn btn-danger', 'data-confirm' => '¿Desea cancelar los cambios?']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
