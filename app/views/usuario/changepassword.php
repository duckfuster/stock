<?php
use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>
<h1>Cambiar Contraseña</h1><br>
<?php $form=ActiveForm::begin([ 'fieldConfig' => ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-4'>{input}</div></div>"]]); ?>
    <?= $form->errorSummary($usuario) ?>
<?= $form->field($usuario,'currentPassword')->passwordInput()?>
<?= $form->field($usuario,'newPassword')->passwordInput()?>
<?= $form->field($usuario,'newPasswordConfirm')->passwordInput()?>

<div class="form-group">
    <div class="col-lg-offset-2 col-lg-10">
        <?= Html::submitButton('Aceptar',['class'=>' btn btn-primary']);?>
    </div>
</div>
<?php ActiveForm::end();?>
