<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\grid\CheckboxColumn;
/* @var $this yii\web\View */
/* @var $model app\models\Rol */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rol-form">

    <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-10'>{input}</div></div>"]]); ?>

    <?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => 20]) ?>

    <h2>Acciones</h2>

    <?= GridView::widget([
        'dataProvider' => $model->ObtenerAcciones(),        
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            ['class' => CheckboxColumn::className(),
            //'header'=>'Disponible',
            'name'=>'seleccion',
            'checkboxOptions'=>['checked'=>true],
            'checkboxOptions' => function($model, $key, $index, $column) {
                return ['value' => $model->IdAccion,'checked' => $model->Disponible];
            },
            ],
            ['attribute' => 'Disponible',
            'visible'=>false
            ],
            ['attribute' => 'IdAccion',
            'visible'=>false],
            'Nombre',            
            ],
        'options'=>[
        'style'=>'height: 400px;overflow: scroll;',
        ],
        ]);
    ?>
    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancelar', Url::toRoute('rol/index'), ['class' => 'btn btn-danger', 'data-confirm' => '¿Desea cancelar los cambios?']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
