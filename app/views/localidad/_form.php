<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Provincia;

/* @var $this yii\web\View */
/* @var $model app\models\Localidad */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="localidad-form">

    <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-4'>{input}</div></div>"],'validateOnSubmit'=>false]); ?>

    <?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'Nombre')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'IdProvincia')->dropDownList(ArrayHelper::map(Provincia::find()->all(),'IdProvincia','Nombre')) ?>

    <?= $form->field($model, 'CodigoPostal',['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-2'>{input}</div></div>"] )->textInput(['maxlength' => 8]) ?>
    
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar', ['class' =>=> $model->isNewRecord ? 'btn btn-success', 'data-confirm' => '¿Desea guardar los cambios?']) ?>
        <?= Html::a('Cancelar', Url::toRoute('localidad/index'), ['class' => 'btn btn-danger', 'data-confirm' => '¿Desea cancelar los cambios?']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
