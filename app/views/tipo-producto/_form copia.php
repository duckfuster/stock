<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\TipoProducto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tipo-producto-form">

   <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-4'>{input}</div></div>"],'validateOnSubmit'=>false]); ?>

    <?= $form->errorSummary($model) ?>

    <?= $form->field($model, 'TipoProducto')->textInput(['maxlength' => 100]) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Guardar' : 'Guardar', ['class' =>'btn btn-success', 'data-confirm' => '¿Desea guardar los cambios?']) ?>
        <?= Html::a('Cancelar', Url::toRoute('tipo-producto/index'), ['class' => 'btn btn-danger', 'data-confirm' => '¿Desea cancelar los cambios?']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>
