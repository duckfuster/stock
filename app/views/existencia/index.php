<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use app\models\Producto;
use app\models\Tipoproducto;
use app\models\Destino;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ExistenciaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Existencia de Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="existencia-index">

    <h2><?= Html::encode($this->title) ?></h2>
    <?php 
    $gridColumns=[          
            /* [
                'attribute' => 'Productos',
                 'filter' => ArrayHelper::map(Producto::find()->orderBy(['Descripcion'=>SORT_ASC])->all(), 'IdProducto', 'Descripcion'),                
             ],*/
       [ 'attribute' => 'IdProducto',
                'vAlign' => 'middle',
                'value' => 'idProducto.Descripcion',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Producto::find()->orderBy(['Descripcion' => SORT_ASC])->asArray()->all(), 'IdProducto', 'Descripcion'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Seleccione'],
            ],
        [ 'attribute' => 'TipoProducto',
                'vAlign' => 'middle',
                'value' => 'idProducto.idTipoProducto.TipoProducto',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(TipoProducto::find()->orderBy(['TipoProducto' => SORT_ASC])->asArray()->all(), 'IdTipoProducto', 'TipoProducto'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Seleccione'],
            ],
        
            'Cantidad',
                                    
    ];
    
   echo GridView::widget([
        'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsive' => true,
                'hover' => true,
                'autoXlFormat' => true,
                //'showPageSummary' => true,
                'panel' => [
                    'type' => GridView::TYPE_DEFAULT,
                    'footer' => false
                ],
                'toolbar' => [
                    '{export}', '{toggleData}'
                ],
                'columns' => $gridColumns,
                'exportConfig' => [
                    GridView::EXCEL => [
                        'filename' => 'existencias_' . date('YmdHis'),
                        'options' => ['title' => 'Microsoft Excel 95+'],
                        'mime' => 'application/vnd.ms-excel',
                    ],
                    GridView::PDF => [
                        'filename' => 'productos_' . date('YmdHis'),
                        'mime' => 'application/pdf',
                        'config' => [
                            'format' => 'A4-P',
                            'destination' => 'I',
                            'mode' => 'utf-8',
                            'methods' => [
                                'SetHTMLHeader' => '<div style="text-aling: left; font-weight: bold;display:inline-block">' . Html::encode($this->title) . '</div>',
                                'SetFooter' => ['{PAGENO}'],
                            ],
                            'cssInline' => '.progress-bar,.progress-bar-warning{background-color:#fff !important;color:#333}'
                        ],
                        'showPageSummary' => FALSE,
                        'showCaption' => FALSE,
                    ]
                ],
                'export' => [
                    'fontAwesome' => false,
                    'showConfirmAlert' => false,
                    'label' => 'Exportar',
                    'target' => GridView::TARGET_BLANK,
                ],
            ]); ?>

</div>
