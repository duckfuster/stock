<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Existencia */

$this->title = $model->IdExistencia;
$this->params['breadcrumbs'][] = ['label' => 'Existencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="existencia-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->IdExistencia], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->IdExistencia], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IdExistencia',
            'IdProducto',
            'Cantidad',
            'TipoMovimiento',
        ],
    ]) ?>

</div>
