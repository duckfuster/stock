<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Existencia */

$this->title = 'Update Existencia: ' . ' ' . $model->IdExistencia;
$this->params['breadcrumbs'][] = ['label' => 'Existencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->IdExistencia, 'url' => ['view', 'id' => $model->IdExistencia]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="existencia-update">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
