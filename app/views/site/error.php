<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = $name;
?>
<div class="site-error">

    <h2><?= Html::encode($this->title) ?></h2>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <p>
        Ocurrio un error mientras el Servidor Web estaba procesando su peticion.
    </p>
    <p>
        Póngase en contacto con nosotros si usted piensa que esto es una error del servidor. Gracias!
    </p>

</div>
