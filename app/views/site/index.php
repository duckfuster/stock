<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Rol;
use app\models\Producto;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\MovimientoProducto;
/* @var $this yii\web\View */

$this->title = 'Divanni';

$session = Yii::$app->session;
$idRol = $session->get('IdRol', 0);

if ($idRol==Rol::OPERADOR){
    $html="<div class=\"col-md-12\">
              <div class=\"well\" style=\"text-align: center;min-width:200px;\">".Html::a('<i style="width:50px;" class="fa fa-truck fa-2x"></i> Salida', Url::to( ['remito/index']), ['class' => 'button-main'])."</div>                      
           </div>";
}else{
    $html="<div class=\"row\">
                    <div class=\"col-md-12\">
                         <div class=\"well\" style=\"text-align: center;min-width:210px;\">".Html::a('<i class="fa fa fa-refresh fa-1x"></i> Movimientos', Url::to( ['movimientoproducto/index']), ['class' => 'button-main'])."</div>
                    </div>                  
               </div>
               <div class=\"row\">
                    <div class=\"col-md-6\">
                        <div class=\"well\" style=\"text-align: center;min-width:200px;\">".Html::a('<i style="width:50px;" class="fa fa-barcode fa-2x"></i> Entrada', Url::to( ['movimientoproducto/create','mov' => 'ent']), ['class' => 'button-main'])."</div>
                    </div>  
                    
                    <!--div class=\"col-md-6\">
                        <div class=\"well\" style=\"text-align: center;min-width:200px;\">".Html::a('<i style="width:50px;" class="fa fa-truck fa-2x"></i> Salida', Url::to( ['movimientoproducto/create','mov' => 'sal']), ['class' => 'button-main'])."</div>                      
                    </div-->
                    
                    <div class=\"col-md-6\">
                        <div class=\"well\" style=\"text-align: center;min-width:200px;\">".Html::a('<i style="width:50px;" class="fa fa-truck fa-2x"></i> Salida', Url::to( ['remito/index']), ['class' => 'button-main'])."</div>                      
                    </div>

                    
                </div>
                 <div class=\"row\">
                    <div class=\"col-md-6\">
                        <div class=\"well\" style=\"text-align: center;min-width:200px;\">".Html::a('<i  class="fa fa-reply fa-1x"></i> Devolución', Url::to( ['movimientoproducto/create','mov' => 'dev']), ['class' => 'button-main'])."</div>
                    </div>  
                    <div class=\"col-md-6\">
                        <div class=\"well\" style=\"text-align: center;min-width:200px;\">".Html::a('<i  class="fa fa-cubes fa-1x"></i> Existencia', Url::to( ['existencia/index']), ['class' => 'button-main'])."</div>                      
                    </div>
                </div>";
    
}

/*
 * SELECT concat(p.descripcion,'  - (',p.codbarra,')') as producto, p.codbarra as codigo,
                  datediff(mp.vencimiento,curdate()) as cantdias
                  FROM movimientoproducto mp 
                  right join producto p on mp.idproducto =p.idproducto 
                  WHERE (mp.vencimiento between curdate() and curdate() + interval 30 day) 
                  and mp.IdDestino=5
                  GROUP BY datediff(mp.vencimiento,curdate())  ASC;
 */

       
    $movimientos= Movimientoproducto::find()
            ->select(["p.descripcion as producto", 'p.codbarra as codigo',
                  'datediff(mp.vencimiento,curdate()) as cantdias'])
            ->from('movimientoproducto mp')
            ->rightJoin('producto p', '`mp`.`idproducto` = `p`.`idproducto`')
            ->where('Vencimiento between curdate() and curdate() + interval 30 day')
            ->groupby('datediff(mp.vencimiento,curdate())  ASC')
            ->asArray()->all();
    $items='';
    foreach ($movimientos as $value){
      
      foreach ($value as $key=>$value2){
         if ($key=='producto') $producto=$value2;
         if ($key=='cantdias') $cantdias=$value2;
         if ($key=='codigo') $codigo=$value2;
        
       }
       if ($idRol==Rol::OPERADOR){
           $items=$items.'<a href="#" class="list-group-item"><i class="fa fa-calendar-check-o"></i>';
       }else{
           $items=$items.'<a href="'.Url::to( ['producto/index','codBarra'=>$codigo]).'" class="list-group-item"><i class="fa fa-calendar-check-o"></i>';
       }
       
       $items=$items.' '.$producto;
       $items=$items.'<span class="pull-right text-muted small"><em>';
       $items=$items.'Vence en '.$cantdias.' dias';
       $items=$items.'</em></span></a>';
      
    }
           
            


// Area Notificaciones
    
    $htmlNotif=' <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> Proximos Vencimientos
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">'.$items.'
                                
                                
                            </div>
                            <!-- /.list-group -->';


?>
<div class="site-index">
    <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Gestion Stock</h1>
                </div>
                <!-- /.col-lg-12 -->
    </div>

    <!--div class="jumbotron">
        <h2>Gestion Stock</h2-->
        <!--?= Html::img('@web/images/gestion-stock.jpg') ?-->
        
            <!--?= GridView::widget([
                    'dataProvider' => $dataProvider,                    
                    'columns' => [
                        'codigo',
                        'cantdias',
                        ['class' => 'yii\grid\ActionColumn','template' => '{update} {delete}',]        ],
                ]); ?-->
           
    <!--/div-->


        <div class="row">
             <div class="col-lg-6">
            <!--div class="col-md-8"-->
                <?php
                    echo($html);
                ?>
               
            </div>
            <div class="col-lg-6">
               <?php
            echo($htmlNotif);
            ?>    
     </div>
        </div>           

</div>
