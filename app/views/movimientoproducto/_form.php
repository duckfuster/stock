<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use app\models\Producto;
use app\models\Destino;
use app\models\Usuario;
use app\models\Motivo;
use yii\jui\AutoComplete;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\MovimientoProducto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="movimiento-producto-form">

    <?= $this->registerJs(
            '$("document").ready(function(){                 
                
                $("input:radio").prop("disabled", function(){ return !this.defaultChecked; });
                
                if($("input:radio[name=\"Movimientoproducto[TipoMovimiento]\"]:checked").val()== 1){
                    $("#vencimiento-producto").show();
                    $("#vencimiento").hide();                
                    $("#precio").hide();                    
                    $("#motivo").hide();
                }
                if($("input:radio[name=\"Movimientoproducto[TipoMovimiento]\"]:checked").val()== 0){
                    $("#motivo").hide();
                }
                
                if($("#tipo-accion").val()==1){
                    $("#fila_prod").hide();
                    $("#btn_mantener").hide();
                    $("#movimientoproducto-fecha").attr("disabled","disabled");
                    
                }
                
                
                if($("#movimientoproducto-iddestino").val()!=0){
                  $("#movimientoproducto-iddestino").attr("disabled","disabled");
                }
                $("#movimientoproducto-idproducto").attr("disabled","disabled");
                $("#w0").submit(function() {
                    $("#movimientoproducto-idproducto").removeAttr("disabled");
                });
                

            
          // declaro orden de tabulacion en el formulario 
            document.getElementById("codbarra").tabIndex = "1";
            document.getElementById("movimientoproducto-cantidad").tabIndex = "2";
            document.getElementById("movimientoproducto-vencimiento").tabIndex = "3";
            document.getElementById("btn_mantener").tabIndex = "4";
            
          // pongo el foco en el codigo barra
            document.getElementById("codbarra").focus();
                
             
              });'
        ); 
    ?>
    

    <?php $form = ActiveForm::begin(['fieldConfig' => ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-4'>{input}</div></div>"],'validateOnSubmit'=>false]); ?>

    <?= $form->errorSummary($model) ?>

    <!--?= $form->field($model, 'CodBarra')->textInput(['maxlength' => true]) ?-->

   <?=$form->field($model, 'Fecha', ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-2'>{input}</div></div>"])
            ->widget(DatePicker::classname(), [
        'language' => 'es',
        'dateFormat' => 'dd-MM-yyyy',        
        'options' => ['class' => 'form-control'],
    ])
    ?>
    
    <div class='row' id="fila_prod"><div class='col-xs-2'><b>Cod. Barra</b></div>
    <?php 
    
    $sql="SELECT p.idproducto as id,concat(p.descripcion,'  - (',p.codbarra,') - ',date_format(mp.vencimiento,'Venc. %d/%m/%y')) as label, 
          p.codbarra as value,datediff(mp.vencimiento,curdate()) as cantdias, e.cantidad as stock, mp.vencimiento as vencimiento 
          FROM movimientoproducto mp 
          right join producto p on mp.idproducto =p.idproducto 
          left join existencia e on p.idproducto = e.idproducto          
          group by p.idproducto,p.codbarra, p.descripcion,mp.vencimiento";
            
    $description_arr=Producto::findBySql($sql)->asArray()->all();
    //echo json_encode($description_arr);

    echo "<div class='col-xs-4'>" ;
    echo AutoComplete::widget([
    'name' => 'CodBarra',
    'id' => 'codbarra',
    'options' => ['class' => 'form-control'],
    'clientOptions' => [
        'source' => $description_arr,       
        'autoFill'=>true,
         'select' => new JsExpression("function( event, ui ) {   
         $('#vencimiento-producto').val(ui.item.cantdias);
         $('#existencia').val(ui.item.stock);
         $('#movimientoproducto-idproducto').val(ui.item.id).selectmenu().selectmenu('refresh', true);   
         $('#movimientoproducto-idproducto').val(ui.item.id);
         if($('input:radio[name=\"Movimientoproducto[TipoMovimiento]\"]:checked').val()== 1){
            $('#movimientoproducto-vencimiento').val(ui.item.vencimiento);
         }
    
     }")],
     ]);
    echo "</div>";
    

    ?>
    
    <div class="col-xs-2">
          <input type="text" id="vencimiento-producto" class="form-control" placeholder="Venc. en dias" readonly="readonly" style="display:none">
        </div>  
    </div>
    <br>
    
    <div class="col-xs-1" style="float: right;margin-right: 45%;">
       <input type="text" tabindex="0" id="existencia" class="form-control col-xs-1" placeholder="stock" readonly="readonly" >
    </div> 
   
    <!--= Html::activeHiddenInput($model, 'IdProducto')?-->
   <?= $form->field($model,'Cantidad',['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-2'>{input}</div></div>"])->textInput(['class' => 'form-control']) ?>
  <div id='precio'>
     <?= $form->field($model,'Precio',['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-2'>{input}</div></div>"])->textInput(['class' => 'form-control']) ?>
  </div>
    <?= $form->field($model, 'IdProducto')->dropDownList(ArrayHelper::map(Producto::find()->all(),'IdProducto','Descripcion'),['prompt'=>'Seleccione']) ?>
    
    <?php
     if ($action==1){
         echo '<input type="hidden" id="tipo-accion" value="1">';
     }else{
          echo '<input type="hidden" id="tipo-accion" value="0">';
     }
    ?>
    <input type="hidden" id="movimientoproducto-idproducto" >
    
    
    
    <?= $form->field($model, 'TipoMovimiento')->radioList(array(0=>'Entrada',1=>'Salida',2=>'Devolución')); ?>

    <?= $form->field($model, 'IdDestino',['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-2'>{input}</div></div>"])->dropDownList(ArrayHelper::map(Destino::find()->all(),'IdDestino','Destino'),['prompt'=>'Seleccione']) ?>

   <div id='vencimiento'>
        <?= $form->field($model, 'Vencimiento', ['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-2'>{input}</div></div>"])
                ->widget(DatePicker::classname(), [
            'language' => 'es',
            'dateFormat' => 'dd-MM-yyyy',
            //'dateFormat' => 'yyyy-MM-dd',
            'options' => ['class' => 'form-control'],
        ])
        ?>
    </div>

    <div id='motivo'>
         <?= $form->field($model, 'IdMotivo',['template' => "<div class='row'><div class='col-xs-2'>{label}</div><div class='col-xs-2'>{input}</div></div>"])->dropDownList(ArrayHelper::map(Motivo::find()->all(),'IdMotivo','Motivo'),['prompt'=>'Seleccione']) ?>        
        </div>  
    </div>  

    <div class="col-xs-16"  style="text-align: center;" >
        <div class="col-xs-1" >
       <?php
       if ($mov=="sal"){
           echo(Html::a("Cancelar", Url::to( ["movimientoproducto/index2","idrem"=>$model->IdRemito]), ["class" => "btn btn-danger", "data-confirm" => "¿Desea cancelar los cambios?"]));
       }else{
           echo(Html::a("Cancelar", Url::to( ["/"]), ["class" => "btn btn-danger", "data-confirm" => "¿Desea cancelar los cambios?"]));
       }
       ?>
        </div>
        
        <div class="col-xs-1" ><?= Html::submitButton($model->isNewRecord ? 'Guardar y Salir' : 'Guardar',['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?></div>
        <div class="col-xs-6" ><?= Html::submitButton($model->isNewRecord ? 'Guardar y Continuar' : 'Guardar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'value'=>'Guardar y Continuar', 'name' => 'btn_mantener','id'=>'btn_mantener']) ?></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
