<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use app\models\Producto;
use app\models\Destino;
use app\models\Rol;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MovimientoProductoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Movimiento Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="movimiento-producto-index">

    <h2><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!-- Html::a('Agregar Movimiento', ['create'], ['class' => 'btn btn-success']) ?-->
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [                                
            [
            'attribute' => 'Fecha',
            'value' => 'Fecha',
            'filter' => DatePicker::widget(['language' => 'es', 'dateFormat' => 'dd-MM-yyyy']),
            'format'=>['DateTime','php:d-m-Y']
            ],
            
           /* ['attribute' => 'Producto',
             'filter' => ArrayHelper::map(Producto::find()->orderBy(['Descripcion'=>SORT_ASC])->all(), 'IdProducto', 'Descripcion'),
            ],*/
            [ 'attribute' => 'IdProducto',
                'vAlign' => 'middle',
                'value' => 'idProducto.Descripcion',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(Producto::find()->orderBy(['Descripcion' => SORT_ASC])->asArray()->all(), 'IdProducto', 'Descripcion'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Seleccione'],
            ],
             'TipoMovimientos',
             'Cantidad',
              'Precio',
            [
                'attribute' => 'Destinos',
                'filter' => ArrayHelper::map(Destino::find()->orderBy(['Destino'=>SORT_ASC])->all(), 'IdDestino', 'Destino'),
            ],
            'Vencimiento',
            //   'Vencimiento',
            //['class' => 'yii\grid\ActionColumn', 'template' => '{update}',]
        ],
    ]); ?>

</div>
