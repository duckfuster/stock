<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use app\models\Producto;
use app\models\Destino;
use app\models\Rol;
use app\models\Remito;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MovimientoProductoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$remito=  Remito::findOne($idrem);

if ($remito!=null){
    $this->title = 'Remito N° '.$remito->Numero;
    $this->params['breadcrumbs'][] = ['label' => 'Remitos', 'url' =>['remito/index']];
    $this->params['breadcrumbs'][] = $this->title;
}else{
    
}

?>
<div class="movimiento-producto-index">

    <h2><?= Html::encode($this->title) ?></h2>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php 
       
        echo(Html::a('Agregar Productos', ['create','mov' => 'sal','idrem'=>$idrem], ['class' => 'btn btn-success']));
        
        ?>
    </p>
     <?php 
     
     if(Rol::checkRol(Rol::DEPOSITO)){
         $gridColumns=[     
            [
            'attribute' => 'Fecha',
            'value' => 'Fecha',
            'filter' => DatePicker::widget(['language' => 'es', 'dateFormat' => 'dd-MM-yyyy']),
            'format'=>['DateTime','php:d-m-Y']
            ],
            ['attribute' => 'Producto',
             'filter' => ArrayHelper::map(Producto::find()->orderBy(['Descripcion'=>SORT_ASC])->all(), 'IdProducto', 'Descripcion'),],
           //  'TipoMovimientos',
             'Cantidad',
            //  'Precio',
            [
                'attribute' => 'Destinos',
                'filter' => ArrayHelper::map(Destino::find()->orderBy(['Destino'=>SORT_ASC])->all(), 'IdDestino', 'Destino'),
            ],
            //   'Vencimiento',
            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}',]
        ];
     }else if(Rol::checkRol(Rol::OPERADOR)){
         $gridColumns=[     
            [
            'attribute' => 'Fecha',
            'value' => 'Fecha',
            'filter' => DatePicker::widget(['language' => 'es', 'dateFormat' => 'dd-MM-yyyy']),
            'format'=>['DateTime','php:d-m-Y']
            ],
            ['attribute' => 'Producto',
             'filter' => ArrayHelper::map(Producto::find()->orderBy(['Descripcion'=>SORT_ASC])->all(), 'IdProducto', 'Descripcion'),],
           //  'TipoMovimientos',
             'Cantidad',
            //  'Precio',
            [
                'attribute' => 'Destinos',
                'filter' => ArrayHelper::map(Destino::find()->orderBy(['Destino'=>SORT_ASC])->all(), 'IdDestino', 'Destino'),
            ],
            //   'Vencimiento',
            //['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}',]
        ];
     }
     
    
        
        
        
    echo GridView::widget([
        'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsive' => true,
                'hover' => true,
                'autoXlFormat' => true,
                'panel' => [
                    'type' => GridView::TYPE_DEFAULT,
                    'footer' => false
                ],
                'toolbar' => [
                    '{export}', '{toggleData}'
                ],
                'columns' => $gridColumns,
                'exportConfig' => [
                    GridView::EXCEL => [
                        'filename' => 'remito_' . date('YmdHis'),
                        'options' => ['title' => 'Microsoft Excel 95+'],
                        'mime' => 'application/vnd.ms-excel',
                    ],
                    GridView::PDF => [
                        'filename' => 'productos_' . date('YmdHis'),
                        'mime' => 'application/pdf',
                        'config' => [
                            'format' => 'A4-P',
                            'destination' => 'I',
                            'mode' => 'utf-8',
                            'methods' => [
                                'SetHTMLHeader' => '<div style="text-aling: left; font-weight: bold;display:inline-block">' . Html::encode($this->title) . '</div>',
                                'SetFooter' => ['{PAGENO}'],
                            ],
                            'cssInline' => '.progress-bar,.progress-bar-warning{background-color:#fff !important;color:#333}'
                        ],
                        'showPageSummary' => FALSE,
                        'showCaption' => FALSE,
                    ]
                ],
                'export' => [
                    'fontAwesome' => false,
                    'showConfirmAlert' => false,
                    'label' => 'Exportar',
                    'target' => GridView::TARGET_BLANK,
                ],
            ]); ?> 
    
     <p>
                        <?= Html::a('Volver', ['remito/index'], ['class' => 'btn btn-primary']) ?>
                        </p>

</div>
