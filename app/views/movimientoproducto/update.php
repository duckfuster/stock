<?php

use yii\helpers\Html;
use app\models\Remito;

/* @var $this yii\web\View */
/* @var $model app\models\MovimientoProducto */

$remito=  Remito::findOne($model->IdRemito);
$this->title = 'Actualizar Producto:';
$this->params['breadcrumbs'][] = ['label' => 'Remitos', 'url' =>['remito/index']];
$this->params['breadcrumbs'][] = ['label' => 'Remito N° '.$remito->Numero, 'url' =>['movimientoproducto/index2','idrem'=>$model->IdRemito]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="movimiento-producto-update">

   <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><?= Html::encode($this->title) ?></h3>
                </div>
                <!-- /.col-lg-12 -->
    </div>
    <?= $this->render('_form', [
        'model' => $model,
        'action'=>'1',
        'mov'=> $model->TipoMovimiento,
    ]) ?>

</div>
