<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MovimientoProducto */

$this->title = $model->IdMovimiento;
$this->params['breadcrumbs'][] = ['label' => 'Movimiento Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="movimiento-producto-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->IdMovimiento], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->IdMovimiento], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'IdMovimiento',
            'CodBarra',
            'Fecha',
            'Cantidad',
            'IdProducto',
            'TipoMovimiento',
            'Destino',
            'Vencimiento',
        ],
    ]) ?>

</div>
