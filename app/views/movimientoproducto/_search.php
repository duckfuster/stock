<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MovimientoProductoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="movimiento-producto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index2'],
        'method' => 'get',
    ]); ?>

    <!--?= $form->field($model, 'IdProducto') ?-->
<div class="col-xs-3"> 
    <?= $form->field($model, 'CodBarra') ?>
</div>
   

    <div class="form-group">
        <br>
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Borrar', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
